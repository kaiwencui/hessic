import Dashboard from "../../../components/authorized-loggedin-verified/dashboard";
import LoadingState from "../../../components/loadingState";
import style from "../../../styles/components/Resource.module.scss"
// import {Button} from "hessic-components"

import { getQuickJS } from "quickjs-emscripten"

// import { Group, Text, useMantineTheme } from '@mantine/core';
// import { IconUpload, IconPhoto, IconX } from '@tabler/icons';
// import { Dropzone, DropzoneProps, IMAGE_MIME_TYPE } from '@mantine/dropzone';
// import Router from "next/router";
// import {message} from "antd";

// const uploadImage = async (options: any) => {
//
//     const res = await fetch(process.env.REACT_APP_BACKEND_URL + '/api/signup', {
//         method: 'POST',
//         headers: {'Content-Type': 'application/json'},
//         body: JSON.stringify(body)
//     }).then(response => response.json())
//
//     switch (res.status) {
//         case 200:
//             // User created successfully
//             await Router.push('/login')
//             setDisabled(true);
//             break;
//         case 201:
//             // New verification token sent
//             message.success('Verification email sent');
//             break;
//         case 500:
//             switch (res.code) {
//                 case 0:
//                     message.error("Error while saving the new user during registration")
//                     break;
//                 case 2:
//                     message.error("A user with this email already exists")
//                     break;
//                 case 3:
//                     message.error("Error while saving the new user during registration")
//                     break;
//                 case 5:
//                     message.error("Error while saving the new user during registration")
//                     break;
//             }
//     }
//
//
//
//
//     fetch(process.env.REACT_APP_BACKEND_URL + "/api/resouce/access/" + window.location.pathname.split("/").pop(), {
//         credentials: 'include' // send cookies to url
//     })
//         .then((r) => r.json())
//         .then((data) => {
//             if (data.status === 200) {
//                 isVerified(true)
//                 // wait 3 seconds and redirect to home page
//                 setTimeout(() => {
//                     Router.push('/', undefined, {shallow:true});
//                 }, 3000);
//             } else {
//                 isVerified(false)
//             }
//         })
// };


export default function Applications(props:any) {

    // const theme = useMantineTheme();


    return (
        <LoadingState>
            <Dashboard title={"Applications"} user={props.user}>

                <iframe className={style.frame} src="/build/index.html" width="100%" height="auto" frameBorder="0"/>

                {/*<Button label="Button"/>*/}

                {/*<Dropzone*/}
                {/*    onDrop={(files) => uploadImage(files)}*/}
                {/*    onReject={(files) => console.log('rejected files', files)}*/}
                {/*    maxSize={3 * 1024 ** 2}*/}
                {/*    accept={IMAGE_MIME_TYPE}*/}
                {/*    {...props}*/}
                {/*>*/}
                {/*    <Group position="center" spacing="xl" style={{ minHeight: 220, pointerEvents: 'none' }}>*/}
                {/*        <Dropzone.Accept>*/}
                {/*            <IconUpload*/}
                {/*                size={50}*/}
                {/*                stroke={1.5}*/}
                {/*            />*/}
                {/*        </Dropzone.Accept>*/}
                {/*        <Dropzone.Reject>*/}
                {/*            <IconX*/}
                {/*                size={50}*/}
                {/*                stroke={1.5}*/}
                {/*                color={theme.colors.red[theme.colorScheme === 'dark' ? 4 : 6]}*/}
                {/*            />*/}
                {/*        </Dropzone.Reject>*/}
                {/*        <Dropzone.Idle>*/}
                {/*            <IconPhoto size={50} stroke={1.5} />*/}
                {/*        </Dropzone.Idle>*/}

                {/*        <div>*/}
                {/*            <Text size="xl" inline>*/}
                {/*                Drag images here or click to select files*/}
                {/*            </Text>*/}
                {/*            <Text size="sm" color="dimmed" inline mt={7}>*/}
                {/*                Attach as many files as you like, each file should not exceed 5mb*/}
                {/*            </Text>*/}
                {/*        </div>*/}
                {/*    </Group>*/}
                {/*</Dropzone>*/}

            </Dashboard>
        </LoadingState>
    )
}