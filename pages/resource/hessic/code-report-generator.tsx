import Dashboard from "../../../components/authorized-loggedin-verified/dashboard";
import LoadingState from "../../../components/loadingState";
import style from "../../../styles/components/Resource.module.scss"

export default function Applications(props:any) {

    return (
        <LoadingState>
            <Dashboard title={"Applications"} user={props.user}>
                <iframe className={style.frame} src="/build2/index.html" width="100%" height="auto" frameBorder="0"/>
                {/*<Button label="Button"/>*/}
            </Dashboard>
        </LoadingState>
    )
}