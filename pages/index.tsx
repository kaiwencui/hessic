import type {NextPage} from 'next'
import React from "react";
import LoadingState from "../components/loadingState";
import {default as AuthorizedLoggedin} from "../components/authorized-loggedin-verified/index";

const Home: NextPage = (props:any) => {
    return (
        <LoadingState>
            <AuthorizedLoggedin user={props.user}/>
        </LoadingState>
    )
}

export default Home