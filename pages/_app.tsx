import '../styles/globals.scss'
import '../styles/layout.scss'
import '../styles/typography.scss'
import '../styles/grid.scss'
import 'antd/dist/antd.css';
import type { AppProps } from 'next/app'

function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}

export default MyApp
