import React, {useEffect, useState} from "react";
import Router from "next/router";
import LoginRegisterStyles from "../../../styles/pages/LoginSignup.module.scss";
import Link from "next/link";

export default function Verification() {

    const [verified, isVerified] = useState(false);

    useEffect(() => {
        fetch(process.env.REACT_APP_BACKEND_URL + "/api/verification/" + window.location.pathname.split("/").pop(), {
            credentials: 'include' // send cookies to url
        })
            .then((r) => r.json())
            .then((data) => {
                if (data.status === 200) {
                    isVerified(true)
                    // wait 3 seconds and redirect to home page
                    setTimeout(() => {
                        Router.push('/', undefined, {shallow:true});
                    }, 3000);
                } else {
                    isVerified(false)
                }
            })
    }, [])

    return (
        <>
            <div className={LoginRegisterStyles.container}>

                {/* Header */}
                <div className={`${LoginRegisterStyles.header}`}>
                    <div className={`cl--90 ${LoginRegisterStyles.headerContent}`}>
                        <Link href="/"><span className={LoginRegisterStyles.logo}>HESSIC</span></Link>
                    </div>
                </div>

                {/* Card */}
                <div className={LoginRegisterStyles.card}>
                    <span className={LoginRegisterStyles.LargeTitle}>Verification status</span>
                    <br></br><br></br>
                    {verified ? (
                        <span>Verification success. Redirecting...</span>
                    ):(<p>Verifying...</p>)}
                </div>
            </div>
        </>
    );
}
