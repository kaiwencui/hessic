import React, {useState} from "react";
import {Button, Form, Input, message} from "antd";
import {ArrowRightOutlined} from "@ant-design/icons";
import styles from "../styles/pages/LoginSignup.module.scss"
import Link from "next/link";
import Router from "next/router";
import {useUser} from "../lib/hooks"

export default function Login() {

    const [errorMsg, setErrorMsg] = useState('')

    const { user } = useUser({
        redirectTo: "/",
        redirectIfFound: true,
    })

    // If user is not fetched yet or user is already logged in
    if (user.isLoggedIn === undefined || user.isLoggedIn === true) {
        return <></>
    }

    async function handleSubmit(values: any) {

        if (errorMsg) setErrorMsg('')

        const body = {
            work_email: values.work_email,
            password: values.password,
        }
        try {
            const res = await fetch(process.env.REACT_APP_BACKEND_URL + "/api/login", {
                method: 'POST',
                credentials: 'include',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(body)
            }).then(response => response.json())

            switch (res.done) {
                case true:
                    await Router.push('/')
                    break;
                case false:
                    message.error('Login Failed: Your email or password is incorrect');
                    break;
            }

        } catch (error: any) {
            console.error('An unexpected error happened occurred:', error)
            setErrorMsg(error.message)
        }
    }

    const onFinishFailed = (errorInfo: any) => {
    };

    return (
        <>
            <div className={styles.container}>

                {/* Header */}
                <div className={`${styles.header}`}>
                    <div className={`cl--90 ${styles.headerContent}`}>
                        <Link href="/"><span className={styles.logo}>HESSIC</span></Link>
                        <span>Don&apos;t have an account? <Link href="/signup">Create an account</Link></span>
                    </div>
                </div>

                {/* Card */}
                <div className={styles.card}>
                    <div className={styles.cardTitle}>Welcome back</div>
                    <h4 className={styles.cardSubtitle}>Sign in to your HESSIC account</h4>
                    <Form
                        initialValues={{remember: true}}
                        onFinish={handleSubmit}
                        onFinishFailed={onFinishFailed}>

                        <h5 className={styles.formItem}>Email</h5>
                        <Form.Item name="work_email">
                            <Input className={"form_entry"}/>
                        </Form.Item>

                        <h5 className={styles.formItem}>Password</h5>
                        <Form.Item name="password">
                            <Input.Password className={"styles.ant-input-suffix"}/>
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit" className={styles.continueButton}>
                                <div style={{textAlign: "left"}}>Continue</div>
                                <div style={{textAlign: "right"}}><ArrowRightOutlined/></div>
                            </Button>
                        </Form.Item>

                        <a href="url">Don&apos;t remember your password?</a>
                    </Form>
                </div>


                {/* Footer */}
                <div className={styles.footerContainer}>
                    <div className={`row center ${styles.footer}`} id={"lg--upper"}>
                        <div className={`cl--40 center ${styles.footerItem}`}>
                            Copyright © 2022 HESSIC
                        </div>
                        <div className={`cl--40 center ${styles.footerItem}`}>
                            <a href={""}>Terms of Use & Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}