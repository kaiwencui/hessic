import React, {useState} from 'react';
import {Button, Checkbox, Form, Input, message} from 'antd';
import ArrowRightOutlined from "@ant-design/icons";
import styles from "../styles/pages/LoginSignup.module.scss";
import Link from "next/link";
import {useUser} from "../lib/hooks";
import Router from "next/router";
import {toaster} from "evergreen-ui";


export default function Signup() {

    let [isDisabled, setDisabled] = useState(false);

    const { user } = useUser({
        redirectTo: "/",
        redirectIfFound: true,
    })

    // If user is not fetched yet or user is already logged in
    if (user.isLoggedIn === undefined || user.isLoggedIn === true) {
        return <></>
    }

    /**
     * Submit registration info
     *
     * @param values - The values from the registration form
     *
     * @todo Handle username or email already taken error
     * @beta
     */
    const onFinish = async (values: any) => {
        const body = {
            firstname: values.firstname,
            lastname: values.lastname,
            company_name: values.company_name,
            work_email: values.work_email,
            password: values.password,
            referral: values.referral,
            hear_about_us: values.hear_about_us,
        }
        try {
            const res = await fetch(process.env.REACT_APP_BACKEND_URL + '/api/signup', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(body)
            }).then(response => response.json())

            switch (res.status) {
                case 200:
                    // User created successfully
                    await Router.push('/login')
                    setDisabled(true);
                    break;
                case 201:
                    // New verification token sent
                    message.success('Verification email sent');
                    break;
                case 500:
                    switch (res.code) {
                        case 0:
                            message.error("Error while saving the new user during registration")
                            break;
                        case 2:
                            message.error("A user with this email already exists")
                            break;
                        case 3:
                            message.error("Error while saving the new user during registration")
                            break;
                        case 5:
                            message.error("Error while saving the new user during registration")
                            break;
                    }
            }
        } catch (error) {
            message.error("Error while saving the new user during registration")
        }
    };

    const onFinishFailed = (errorInfo: any) => {
    };

    return (
        <>
            <div className={styles.container}>

                {/* Top bar */}
                <div className={`${styles.header}`}>
                    <div className={`cl--80 ${styles.headerContent}`}>
                        <Link href="/hessic_front-end-official/pages"><span className={styles.logo}>HESSIC</span></Link>
                        <span>Have an account? <Link href="/login"><a>Log in</a></Link></span>
                    </div>
                </div>

                {/* Body */}
                <div className={styles.card}>
                    <div className={styles.cardTitle}>Welcome to HESSIC</div>
                    <h4 className={styles.cardSubtitle}>Create your account</h4>

                    <Form
                        initialValues={{remember: true}}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}>

                        <div className={"row space-between"}>
                            <div className={"cl--48"}>
                                <h5 className={styles.formItem}>Legal first name</h5>
                                <Form.Item name="firstname" rules={[{ required: true, message: 'This is a required field' }]}>
                                    <Input className={"form_entry"}/>
                                </Form.Item>
                            </div>

                            <div className={"cl--48"}>
                                <h5 className={styles.formItem}>Legal last name</h5>
                                <Form.Item name="lastname" rules={[{ required: true, message: 'This is a required field' }]}>
                                    <Input className={"form_entry"}/>
                                </Form.Item>
                            </div>
                        </div>

                        <h5 className={styles.formItem}>Company name</h5>
                        <Form.Item name="company_name" rules={[{ required: true, message: 'This is a required field' }]}>
                            <Input className={"form_entry"}/>
                        </Form.Item>

                        <h5 className={styles.formItem}>Work email</h5>
                        <Form.Item name="work_email" rules={[{ required: true, message: 'This is a required field' }]}>
                            <Input className={"form_entry"}/>
                        </Form.Item>

                        <h5 className={styles.formItem}>Password</h5>
                        <Form.Item name="password" rules={[{ required: true, message: 'This is a required field' }]}>
                            <Input.Password className={"styles.ant-input-suffix"}/>
                        </Form.Item>

                        <h5 className={styles.formItem}>Referral code (optional)</h5>
                        <Form.Item name="referral">
                            <Input className={"form_entry"}/>
                        </Form.Item>

                        <h5 className={styles.formItem}>How did you hear about us? (optional)</h5>
                        <Form.Item name="hear_about_us">
                            <Input className={"form_entry"}/>
                        </Form.Item>

                        <Form.Item
                            name="agreement"
                            valuePropName="checked"
                            className={styles.checkBox}
                            rules={[
                                {
                                    validator: (_, value) =>
                                        value ? Promise.resolve() : Promise.reject(new Error('Should accept agreement')),
                                },
                            ]}
                        >
                            <Checkbox className={"xs--text"}>
                                By checking this box, I acknowledge and agree to the terms of the <a href={""}>HESSIC Platform Agreement </a>
                                on behalf of the Company identified above, that I am authorized to do so on behalf of the Company, and that I have reviewed the terms of the
                                <a href={""}> HESSIC Privacy Policy</a>.
                            </Checkbox>
                        </Form.Item>

                        <Form.Item>
                            {isDisabled ? (
                                <Button disabled={true} type="primary" htmlType="submit"
                                        style={{width: "100%", textAlign: "left", display: "flex", color: "white", backgroundColor: "grey"}}>
                                    <div style={{textAlign: "left"}}>Registration Link Sent!</div>
                                    <div style={{textAlign: "right"}}><ArrowRightOutlined/></div>
                                </Button>
                            ):(
                                <Button type="primary" htmlType="submit"
                                        style={{width: "100%", textAlign: "left", display: "flex"}}>
                                    <div style={{textAlign: "left"}}>Get Registration Link</div>
                                </Button>
                            )}
                        </Form.Item>
                    </Form>

                </div>

                {/* Footer */}
                <div className={styles.footerContainer}>
                    <div className={`row center ${styles.footer}`} id={"lg--upper"}>
                        <div className={`cl--40 center ${styles.footerItem}`}>
                            Copyright © 2022 HESSIC
                        </div>
                        <div className={`cl--40 center ${styles.footerItem}`}>
                            <a href={""}>Terms of Use & Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
