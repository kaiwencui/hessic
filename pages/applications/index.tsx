import MyApplicationStyle from "../../styles/pages/MyApplications.module.scss"
import Router from "next/router";
import {TokensIcon} from "@radix-ui/react-icons";
import AppBrowse from "../../styles/pages/AppBrowse.module.scss";
import Dashboard from "../../components/authorized-loggedin-verified/dashboard";
import LoadingState from "../../components/loadingState";

export default function Applications(props:any) {

  return (
      <LoadingState>
          <Dashboard title={"Applications"} user={props.user}>
              <div className={MyApplicationStyle.contentContainer}>
                  <h1 className={AppBrowse.headerTitle}>Discover new applications.</h1>
                  <button className={"button1"} onClick={() => Router.push("/applications/browse")}>Browse all apps
                  </button>
              </div>
              <div className={AppBrowse.contentContainer}>
                  <h1>My Applications</h1>
              </div>
              <div className={MyApplicationStyle.pageViewContent}>
                  <div className={MyApplicationStyle.tilesContainer}>
                      <div className={MyApplicationStyle.tilesGrid}>
                          {/* App Container*/}
                          <div className={MyApplicationStyle.tileContentContainer}>
                              <div className={MyApplicationStyle.genericTile}>
                                  <div className={MyApplicationStyle.tileTitleBorder}/>
                                  <div className={MyApplicationStyle.genericTileGridView}>
                                      <div className={MyApplicationStyle.genericTileImage}>
                                          <div className={MyApplicationStyle.genericTileThumbnail}/>
                                      </div>
                                  </div>
                                  <div className={MyApplicationStyle.genericTileLower}>
                                      <div className={MyApplicationStyle.tileIconContainer}>
                                          <TokensIcon className={MyApplicationStyle.tileIcon}/>
                                      </div>
                                      <div className={MyApplicationStyle.genericTileLowerContent}>
                                          <div className={MyApplicationStyle.genericTileLowerContentTitle}>
                                              Untitled
                                          </div>
                                          <div className={MyApplicationStyle.genericTileLowerContentSubtitle}>
                                              Description
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </Dashboard>
      </LoadingState>
  )
}
