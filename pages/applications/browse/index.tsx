import React, {useEffect, useRef, useState} from 'react';
import AppBrowse from "../../../styles/pages/AppBrowse.module.scss";
import Router from "next/router";
import Dashboard from "../../../components/authorized-loggedin-verified/dashboard";
import LoadingState from "../../../components/loadingState";
import { useForm, Controller } from "react-hook-form";
import {FileUploader as FU} from "baseui/file-uploader";
import {BaseProvider, LightTheme} from 'baseui';
import { Provider as StyletronProvider } from "styletron-react";
import dynamic from 'next/dynamic'


// Dialog
import { styled, keyframes } from '@stitches/react';
import { violet, blackA, mauve, green } from '@radix-ui/colors';
import { Cross2Icon } from '@radix-ui/react-icons';
import * as DialogPrimitive from '@radix-ui/react-dialog';

/**
 * Dialog
 **/

const overlayShow = keyframes({
    '0%': { opacity: 0 },
    '100%': { opacity: 1 },
});
const contentShow = keyframes({
    '0%': { opacity: 0, transform: 'translate(-50%, -48%) scale(.96)' },
    '100%': { opacity: 1, transform: 'translate(-50%, -50%) scale(1)' },
});
const StyledOverlay = styled(DialogPrimitive.Overlay, {
    backgroundColor: blackA.blackA9,
    position: 'fixed',
    zIndex: 3,
    inset: 0,
    '@media (prefers-reduced-motion: no-preference)': {
        animation: `${overlayShow} 150ms cubic-bezier(0.16, 1, 0.3, 1)`,
    },
});
const StyledContent = styled(DialogPrimitive.Content, {
    backgroundColor: 'white',
    borderRadius: 6,
    boxShadow: 'hsl(206 22% 7% / 35%) 0px 10px 38px -10px, hsl(206 22% 7% / 20%) 0px 10px 20px -15px',
    position: 'fixed',
    zIndex: 4,
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '90vw',
    maxWidth: '450px',
    maxHeight: '85vh',
    padding: 25,
    '@media (prefers-reduced-motion: no-preference)': {
        animation: `${contentShow} 150ms cubic-bezier(0.16, 1, 0.3, 1)`,
    },
    '&:focus': { outline: 'none' },
});
function Content({ children, ...props }:any) {
    return (
        <DialogPrimitive.Portal>
            <StyledOverlay  />
            <StyledContent {...props}>{children}</StyledContent>
        </DialogPrimitive.Portal>
    );
}
const StyledTitle = styled(DialogPrimitive.Title, {
    margin: 0,
    fontWeight: 500,
    color: mauve.mauve12,
    fontSize: 17,
});
const StyledDescription = styled(DialogPrimitive.Description, {
    margin: '10px 0 20px',
    color: mauve.mauve11,
    fontSize: 15,
    lineHeight: 1.5,
});
export const Dialog = DialogPrimitive.Root;
export const DialogTrigger = DialogPrimitive.Trigger;
export const DialogContent = Content;
export const DialogTitle = StyledTitle;
export const DialogDescription = StyledDescription;
export const DialogClose = DialogPrimitive.Close;
const Flex = styled('div', { display: 'flex' });
const Box = styled('div', {});
const Button = styled('button', {
    all: 'unset',
    display: 'inline-flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    padding: '0 15px',
    fontSize: 15,
    lineHeight: 1,
    fontWeight: 500,
    height: 35,
    variants: {
        variant: {
            violet: {
                backgroundColor: 'white',
                color: violet.violet11,
                boxShadow: `0 2px 10px ${blackA.blackA7}`,
                '&:hover': { backgroundColor: mauve.mauve3 },
                '&:focus': { boxShadow: `0 0 0 2px black` },
            },
            green: {
                backgroundColor: green.green4,
                color: green.green11,
                '&:hover': { backgroundColor: green.green5 },
                '&:focus': { boxShadow: `0 0 0 2px ${green.green7}` },
            },
        },
    },
    defaultVariants: {
        variant: 'violet',
    },
});
const IconButton = styled('button', {
    all: 'unset',
    fontFamily: 'inherit',
    borderRadius: '100%',
    height: 25,
    width: 25,
    display: 'inline-flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: violet.violet11,
    position: 'absolute',
    top: 10,
    right: 10,
    '&:hover': { backgroundColor: violet.violet4 },
    '&:focus': { boxShadow: `0 0 0 2px ${violet.violet7}` },
});
const Fieldset = styled('fieldset', {
    all: 'unset',
    display: 'flex',
    gap: 20,
    alignItems: 'center',
    marginBottom: 15,
});
const Input = styled('input', {
    all: 'unset',
    width: '100%',
    flex: '1',
    display: 'inline-flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    padding: '0 10px',
    fontSize: 15,
    lineHeight: 1,
    color: violet.violet11,
    boxShadow: `0 0 0 1px ${violet.violet7}`,
    height: 35,

    '&:focus': { boxShadow: `0 0 0 2px ${violet.violet8}` },
});

export default function Index(props:any) {

    const [isUploading, setIsUploading] = useState(false);
    const [uploadedFileName, setUploadedFileName] = useState("");
    const timeoutId = useRef<any>();
    function reset() {
        setIsUploading(false);
        clearTimeout(timeoutId.current);
    }
    const { register, handleSubmit, control, formState: { errors } } = useForm();
    function startProgress(acceptedFiles:any) {
        setIsUploading(true);
        timeoutId.current = setTimeout(reset, 800);
        setUploadedFileName(acceptedFiles[0].name)
    }
    const onSubmit = async (data:any) => {

        // Send file to server
        const dataToSend = new FormData()
        dataToSend.append('name', data['project_name'])
        dataToSend.append('author', data['project_author'])
        dataToSend.append('version', data['project_version'])
        dataToSend.append('source', data['project_url'])
        dataToSend.append('file', data['uploadFile'][0])
        const res = await fetch("http://127.0.0.1:5000" + "/api/v1/utils/codetopdf", {
            method: 'POST',
            body: dataToSend
        })  .then((response) => response.blob())
            .then((blob) => {
                // Create blob link to download
                const url = window.URL.createObjectURL(
                    new Blob([blob]),
                );

                const url2 = new Blob([blob], {type: 'application/pdf'});
                const url3 = URL.createObjectURL(url2);

            });
    }

    function onDelete() {
        setUploadedFileName("")
    }

    return (
        <LoadingState>
            <Dashboard title={"Browse applications"} user={props.user}>
                <div className={AppBrowse.contentContainer}>
                    <h1 className={AppBrowse.headerTitle}>Applications</h1>
                    <p className={AppBrowse.capitalizedSubText}>CUSTOM-TAILORED FOR EVERY NEEDS</p>
                    <p className={AppBrowse.descriptionText}>HESSIC Technologies has researched and developed over 30 applications. Apps are constantly updated and delivered with the latest state-of-the-art technology.</p>
                </div>
                <div className={AppBrowse.pageViewContent}>
                    <div className={AppBrowse.boxContainer}>
                        <div className={AppBrowse.contentContainer}>
                            <h2 className={AppBrowse.subheaderText}>Browse by category
                                <a href="/applications/browse/category/machine-learning/index.tsx" className={AppBrowse.seeAllLink}>See all</a>
                            </h2>
                            <div className={AppBrowse.scrollableDiv}>
                                <a className={AppBrowse.gradientBox} onClick={()=> Router.push("/applications/browse/category/machine-learning")}>
                                    <div className={AppBrowse.gradientBoxLeftSide}>
                                        <div className={AppBrowse.gradientBoxLeftSideTextSmall}>
                                            3 resources
                                        </div>
                                        <div className={AppBrowse.gradientBoxLeftSideTextBig}>
                                            Machine Learning
                                        </div>
                                    </div>
                                </a>
                                <a className={AppBrowse.gradientBox2}>
                                    <div className={AppBrowse.gradientBoxLeftSide}>
                                        <div className={AppBrowse.gradientBoxLeftSideTextSmall}>
                                            5 resources
                                        </div>
                                        <div className={AppBrowse.gradientBoxLeftSideTextBig}>
                                            Data Science
                                        </div>
                                    </div>
                                </a>
                                <a className={AppBrowse.gradientBox3}>
                                    <div className={AppBrowse.gradientBoxLeftSide}>
                                        <div className={AppBrowse.gradientBoxLeftSideTextSmall}>
                                            5 resources
                                        </div>
                                        <div className={AppBrowse.gradientBoxLeftSideTextBig}>
                                            Business Intelligence
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={AppBrowse.pageViewContent}>
                    <div className={AppBrowse.boxContainer}>
                        <div className={AppBrowse.contentContainer}>
                            <h2 className={AppBrowse.subheaderText}>Browse by industry
                                <a href="/applications/browse/industry" className={AppBrowse.seeAllLink}>See all</a>
                            </h2>
                            <div className={AppBrowse.scrollableDiv}>
                                <a className={AppBrowse.gradientBox}>
                                    <div className={AppBrowse.gradientBoxLeftSide}>
                                        <div className={AppBrowse.gradientBoxLeftSideTextSmall}>
                                            3 resources
                                        </div>
                                        <div className={AppBrowse.gradientBoxLeftSideTextBig}>
                                            Finance
                                        </div>
                                    </div>
                                </a>
                                <a className={AppBrowse.gradientBox2}>
                                    <div className={AppBrowse.gradientBoxLeftSide}>
                                        <div className={AppBrowse.gradientBoxLeftSideTextSmall}>
                                            5 resources
                                        </div>
                                        <div className={AppBrowse.gradientBoxLeftSideTextBig}>
                                            Engineering
                                        </div>
                                    </div>
                                </a>
                                <a className={AppBrowse.gradientBox3}>
                                    <div className={AppBrowse.gradientBoxLeftSide}>
                                        <div className={AppBrowse.gradientBoxLeftSideTextSmall}>
                                            5 resources
                                        </div>
                                        <div className={AppBrowse.gradientBoxLeftSideTextBig}>
                                            Aerospace
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <Dialog>
                    <div className={`${AppBrowse.contentContainer} ${AppBrowse.custom}`}>
                        <h1 className={AppBrowse.headerTitle2}>Custom Applications</h1>
                        <div>
                            <p className={AppBrowse.capitalizedSubText2}>UPLOAD APPLICATION</p>
                            <p className={AppBrowse.descriptionText}>Upload <DialogTrigger asChild><a href={"#"}>here</a></DialogTrigger> your custom HESSIC application.</p>
                            <p className={AppBrowse.capitalizedSubText2}>Developer SDK</p>
                            <p className={AppBrowse.descriptionText}>With our software development kit, you can easily create your own application and integrate it into the HESSIC ecosystem. <a href={""}>Learn more</a>.</p>
                        </div>
                        <DialogContent >
                            <DialogTitle>Publish applications to the HESSIC platform</DialogTitle>
                            <DialogDescription>
                                When you are ready to share your application you can submit your plugin to the HESSIC Platform. This allows other users to install and run your application(s).
                            </DialogDescription>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                {/*<Label*/}
                                {/*    label_name="Project name"*/}
                                {/*    label_id="project_name"*/}
                                {/*    register={register}*/}
                                {/*    required={true}*/}
                                {/*/>*/}
                                {/*<Label*/}
                                {/*    label_name="Author"*/}
                                {/*    label_id="project_author"*/}
                                {/*    register={register}*/}
                                {/*    required={true}*/}
                                {/*/>*/}
                                {/*<Label*/}
                                {/*    label_name="Version"*/}
                                {/*    label_id="project_version"*/}
                                {/*    register={register}*/}
                                {/*    required={true}*/}
                                {/*/>*/}
                                {/*<Label*/}
                                {/*    label_name="Source url"*/}
                                {/*    label_id="project_url"*/}
                                {/*    register={register}*/}
                                {/*    required={true}*/}
                                {/*/>*/}
                                {/*<Controller*/}
                                {/*    control={control}*/}
                                {/*    name="uploadFile"*/}
                                {/*    render={({ field }) => (*/}
                                {/*        // <StyletronProvider value={engine}>*/}
                                {/*        //     <BaseProvider theme={LightTheme}>*/}
                                {/*        //         {uploadedFileName == "" ?*/}
                                {/*        //             <FU*/}
                                {/*        //                 onCancel={reset}*/}
                                {/*        //                 onDrop={(acceptedFiles, rejectedFiles) => {*/}
                                {/*        //                     field.onChange(acceptedFiles);*/}
                                {/*        //                     startProgress(acceptedFiles);*/}
                                {/*        //                 }}*/}
                                {/*        //                 progressMessage={*/}
                                {/*        //                     isUploading ? `Uploading... hang tight.` : ''*/}
                                {/*        //                 }*/}
                                {/*        //             />: null}*/}
                                {/*        //     </BaseProvider>*/}
                                {/*        //     {uploadedFileName != "" ? <UploadedCard style={{marginTop: "20px"}}*/}
                                {/*        //                                             fileName={uploadedFileName}*/}
                                {/*        //                                             onDelete={()=> {*/}
                                {/*        //                                                 onDelete();*/}
                                {/*        //                                                 field.onChange("");*/}
                                {/*        //                                             } }/> : null}*/}
                                {/*        // </StyletronProvider>*/}
                                {/*    )}*/}
                                {/*/>*/}
                            </form>
                            <DialogClose asChild>
                                <IconButton aria-label="Close">
                                    <Cross2Icon />
                                </IconButton>
                            </DialogClose>
                        </DialogContent>
                    </div>
                </Dialog>
            </Dashboard>
        </LoadingState>
    )
}