import React from 'react';
import AppBrowse from "../../../../../styles/pages/AppBrowse.module.scss";
import MyApplicationStyle from "../../../../../styles/pages/MyApplications.module.scss"
import {TokensIcon} from "@radix-ui/react-icons";
import Dashboard from "../../../../../components/authorized-loggedin-verified/dashboard";
import LoadingState from "../../../../../components/loadingState";

export default function Index(props:any) {

    return (
        <LoadingState>
            <Dashboard user={props} title={"Machine Learning Applications"}>
                <div className={AppBrowse.contentContainer}>
                    <h1 className={AppBrowse.headerTitle}>Machine Learning Applications</h1>
                    <p className={AppBrowse.capitalizedSubText}>GLOSSARY</p>
                    <p className={AppBrowse.descriptionText}>Machine learning is a branch of artificial intelligence (AI) and computer science which focuses on the use of data and algorithms to imitate the way that humans learn, gradually improving its accuracy.</p>
                </div>
                <div className={MyApplicationStyle.pageViewContent}>
                    <div className={MyApplicationStyle.tilesContainer}>
                        <div className={MyApplicationStyle.tilesGrid}>
                            {/* App Container*/}
                            <div className={MyApplicationStyle.tileContentContainer}>
                                <div className={MyApplicationStyle.genericTile}>
                                    <div className={MyApplicationStyle.tileTitleBorder}/>
                                    <div className={MyApplicationStyle.genericTileGridView}>
                                        <div className={MyApplicationStyle.genericTileImage}>
                                            <div className={MyApplicationStyle.genericTileThumbnail}/>
                                        </div>
                                    </div>
                                    <div className={MyApplicationStyle.genericTileLower}>
                                        <div className={MyApplicationStyle.tileIconContainer}>
                                            <TokensIcon className={MyApplicationStyle.tileIcon}/>
                                        </div>
                                        <div className={MyApplicationStyle.genericTileLowerContent}>
                                            <div className={MyApplicationStyle.genericTileLowerContentTitle}>
                                                Clothing Segmentation
                                            </div>
                                            <div className={MyApplicationStyle.genericTileLowerContentSubtitle}>
                                                Description
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </Dashboard>
        </LoadingState>
    )
}