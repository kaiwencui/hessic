import React from 'react';
import AppBrowse from "../../../../../styles/pages/AppBrowse.module.scss";
import Dashboard from "../../../../../components/authorized-loggedin-verified/dashboard";
import LoadingState from "../../../../../components/loadingState";
import {RocketIcon} from "@radix-ui/react-icons";

export default function ImageSegmentation(props:any) {

    return (
        <LoadingState>
            <Dashboard user={props.user} title={"Machine Learning Applications"}>


                {/* Header Desktop */}
                <div className={AppBrowse.AppHeader}>
                    <div className={AppBrowse.AppHeaderContent}>
                        <div className={AppBrowse.AppHeaderLeft}>
                            <div className={AppBrowse.AppHeaderLeftTitle}>
                                Image segmentation
                            </div>
                            <div className={AppBrowse.AppHeaderLeftSubTitle}>
                                By HESSIC Technologies
                            </div>
                            <div className={AppBrowse.AppHeaderLeftSubTitleDescription}>
                                Image segmentation is the process of assigning a label to every pixel in an image such that pixels with the same label share certain characteristics.
                            </div>
                        </div>

                        <div className={AppBrowse.AppHeaderRight}>
                            <div className={AppBrowse.AppHeaderRightButtonContainer}>
                                <button className={AppBrowse.AppHeaderRightButton}>
                                    Deploy <RocketIcon className={AppBrowse.iconButton}/>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>

                {/* Header Mobile */}
                <div className={AppBrowse.AppHeaderMobile}>
                    <div className={AppBrowse.AppHeaderLeftTitle}>
                        Image segmentation
                    </div>
                    <div className={AppBrowse.AppHeaderLeftSubTitle}>
                        By HESSIC Technologies
                    </div>
                    <div className={AppBrowse.AppHeaderLeftSubTitleDescription}>
                        Image segmentation is the process of assigning a label to every pixel in an image such that pixels with the same label share certain characteristics.
                    </div>
                    <div className={AppBrowse.AppHeaderButtonMobile}>
                        <div className={AppBrowse.AppHeaderRightButtonContainer}>
                            <button className={AppBrowse.AppHeaderRightButton}>
                                Deploy <RocketIcon className={AppBrowse.iconButton}/>
                            </button>
                        </div>
                    </div>
                </div>

                <div className={AppBrowse.AppDetailsContainer}>
                    <div className={AppBrowse.imageCover}>
                        <img src={"https://s3-alpha-sig.figma.com/plugins/1151890004010191690/39748/7fa14805-fe2b-41b1-b6d5-fbdfad97f7e5-cover?Expires=1665360000&Signature=AQ0e9P3clDcCa7UscFmGnjJ~cL~c6MbcULb68e0SRXRRKr00SnhFelR8b19CWmsn1Jg~4Hxe17tL-uMAlNSsN11A8QPZVQ4DZ9gHLfp5-rkGjxO-Ccn9LDMMwn-QSUWLBg81CkAiirE6j33l5zOfV~sADItuMcwIYRwPbIPBPDwLSeemILDsJRpOFJ2unTKE4UTuScsj3CPgWczzNMnfEXFweYWh1Z9sZbvUtX~8c1~2KovJ4-k4h1CUkRVhjHb6CCjW40iWBpNWtL80r52Nndt39EyJb9Iu8iC~f-JUkn59nUuViy181GTCEvZ6hh3i5cnIkQ4nMOq0pCmqcXfc1w__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"} alt={"Image segmentation"}/>
                    </div>

                    <div className={AppBrowse.metadataContainer}>
                        <div className={AppBrowse.metadataContainerLeft}>
                            <div className={AppBrowse.metadataContainerLeftVersion}>
                                <div className={AppBrowse.metadataLeftVersion}>
                                    <div className={AppBrowse.metadataLeftTitle}>
                                        Version history
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className={AppBrowse.metadataLeftSubTitle}>
                                    Version 2 on 26 September 2022
                                </div>
                            </div>
                        </div>

                        <div className={AppBrowse.metadataContainerRight}>
                            <div className={AppBrowse.metadataContainerRightSection}>
                                <div>Last updated 4 days ago</div>
                                <div>Support: app-support@hessic.com</div>
                            </div>
                        </div>
                    </div>

                </div>
            </Dashboard>
        </LoadingState>
    )
}