exports.id = 807;
exports.ids = [807];
exports.modules = {

/***/ 763:
/***/ ((module) => {

// Exports
module.exports = {
	"container": "Dashboard_container__ajjXg",
	"flex_container": "Dashboard_flex_container__jxb5_",
	"left_container": "Dashboard_left_container__mjdVa",
	"left_container_content": "Dashboard_left_container_content__GUhy7",
	"right_content_container": "Dashboard_right_content_container__To94A",
	"projects": "Dashboard_projects__sWuFl",
	"projects_title": "Dashboard_projects_title__qcNHq",
	"projects_title_item": "Dashboard_projects_title_item__dntxe",
	"right": "Dashboard_right__RLZGd",
	"projects_content": "Dashboard_projects_content__dnUAw",
	"activity_title": "Dashboard_activity_title__hFzU4",
	"activity_content": "Dashboard_activity_content__LBCBr",
	"banner": "Dashboard_banner__dojd_",
	"PageViewContainer": "Dashboard_PageViewContainer__vQcle"
};


/***/ }),

/***/ 3838:
/***/ ((module) => {

// Exports
module.exports = {
	"navbarContainer": "Navbar_navbarContainer__7neQ6",
	"workSpaceName": "Navbar_workSpaceName__LEeI5",
	"workSpaceIcon": "Navbar_workSpaceIcon__jhg7E",
	"workSpaceNameTitleSmaller": "Navbar_workSpaceNameTitleSmaller__SOdCk",
	"workSpaceNameTitleSubtitle": "Navbar_workSpaceNameTitleSubtitle__tLvvb",
	"navbarContentContainer": "Navbar_navbarContentContainer__ERgbT",
	"leftNav": "Navbar_leftNav__UTJBl",
	"buttonUser": "Navbar_buttonUser__PG6Im",
	"navbarContentContainerActive": "Navbar_navbarContentContainerActive__E1ffJ",
	"navbarContent": "Navbar_navbarContent__pvpkx",
	"smallIconContainer": "Navbar_smallIconContainer__xsWM6",
	"small": "Navbar_small__3t6x7",
	"dropdownIcon": "Navbar_dropdownIcon__WxHp8",
	"dropdownMenu": "Navbar_dropdownMenu__9Kvys",
	"dropdownMenuUsername": "Navbar_dropdownMenuUsername__2JeAY",
	"dropdownMenuUsernameIcon": "Navbar_dropdownMenuUsernameIcon__eRvCF",
	"dropdownMenuUsernameEmail": "Navbar_dropdownMenuUsernameEmail__PHHLc",
	"dropdownMenuItem": "Navbar_dropdownMenuItem__7Y0Zf"
};


/***/ }),

/***/ 8955:
/***/ ((module) => {

// Exports
module.exports = {
	"navDefault": "Sidebar_navDefault__BDkzl",
	"scrollContainer": "Sidebar_scrollContainer__hj6rq",
	"scrollContainerWithOverscroll": "Sidebar_scrollContainerWithOverscroll__Axb8J",
	"innerScrollContainer": "Sidebar_innerScrollContainer__tw8LV",
	"navContent": "Sidebar_navContent__HXqpy",
	"sidebarSection": "Sidebar_sidebarSection__P4FKb",
	"sidebarIconContainer": "Sidebar_sidebarIconContainer__RrmZV",
	"sidebarIcon": "Sidebar_sidebarIcon__ioyLt"
};


/***/ }),

/***/ 1923:
/***/ ((module) => {

// Exports
module.exports = {
	"toolbarSpacerContainer": "Toolbar_toolbarSpacerContainer__fl8ao",
	"toolbarSpacerContainerMobile": "Toolbar_toolbarSpacerContainerMobile__zuWF_",
	"toolbarMobileLeft": "Toolbar_toolbarMobileLeft__5yI3_",
	"toolbarMobileButtonMenu": "Toolbar_toolbarMobileButtonMenu__y7_LP",
	"toolbarMobileTitle": "Toolbar_toolbarMobileTitle__AaYX8"
};


/***/ }),

/***/ 2477:
/***/ ((module) => {

// Exports
module.exports = {
	"container": "Homepage_container__EHm3X",
	"layout": "Homepage_layout__jrr_S",
	"parallax": "Homepage_parallax__Khxf4",
	"background": "Homepage_background__ER3JT",
	"logo": "Homepage_logo__THZLU",
	"navbar": "Homepage_navbar__JNxce",
	"navbarDesktop": "Homepage_navbarDesktop__1EEU5",
	"navbarDesktopComponent": "Homepage_navbarDesktopComponent__CAdec",
	"navbarDesktopComponentsRight": "Homepage_navbarDesktopComponentsRight__Tm_gX",
	"title": "Homepage_title__fGFE6",
	"subTitle": "Homepage_subTitle__og9JC",
	"description": "Homepage_description__vADoQ",
	"parallaxText": "Homepage_parallaxText__cxJzU",
	"comingSoon": "Homepage_comingSoon__vp_NY",
	"parallaxImage": "Homepage_parallaxImage__mVdI7",
	"backgroundImage2": "Homepage_backgroundImage2__5aUn8",
	"parallaxComing": "Homepage_parallaxComing__Tougx",
	"footer": "Homepage_footer__OVbzu",
	"footerGrid": "Homepage_footerGrid__hKk9s",
	"copyright": "Homepage_copyright__vnhjQ",
	"copyrightRow": "Homepage_copyrightRow__Ksq0h",
	"termsOfUse": "Homepage_termsOfUse__XsgWk",
	"navbarMobile": "Homepage_navbarMobile__nCTst"
};


/***/ }),

/***/ 5746:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ Dashboard)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5725);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(antd__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_components_Dashboard_module_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(763);
/* harmony import */ var _styles_components_Dashboard_module_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_styles_components_Dashboard_module_scss__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _sidebar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7705);
/* harmony import */ var _navbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(7655);
/* harmony import */ var _toolBar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(7590);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_navbar__WEBPACK_IMPORTED_MODULE_4__]);
_navbar__WEBPACK_IMPORTED_MODULE_4__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];







function Dashboard(props) {
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(antd__WEBPACK_IMPORTED_MODULE_2__.Layout, {
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_navbar__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .ZP, {
                user: props.user
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_sidebar__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {}),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: (_styles_components_Dashboard_module_scss__WEBPACK_IMPORTED_MODULE_6___default().PageViewContainer),
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_toolBar__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                        title: props.title
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "pageViewContent",
                        children: props.children
                    })
                ]
            })
        ]
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 7655:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ZP": () => (/* binding */ Navbar)
/* harmony export */ });
/* unused harmony exports DropdownMenu, DropdownMenuTrigger, DropdownMenuContent, DropdownMenuItem, DropdownMenuSeparator, DropdownMenuSub, DropdownMenuSubTrigger, DropdownMenuSubContent */
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3838);
/* harmony import */ var _styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _stitches_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(796);
/* harmony import */ var _radix_ui_colors__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6366);
/* harmony import */ var _radix_ui_colors__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_radix_ui_colors__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2132);
/* harmony import */ var _radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _radix_ui_react_dropdown_menu__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8532);
/* harmony import */ var _radix_ui_react_dropdown_menu__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_radix_ui_react_dropdown_menu__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var evergreen_ui__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4371);
/* harmony import */ var evergreen_ui__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(evergreen_ui__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _lib_user__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(5590);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_stitches_react__WEBPACK_IMPORTED_MODULE_2__]);
_stitches_react__WEBPACK_IMPORTED_MODULE_2__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];










/**
 * Start Radix UI Dropdown Menu
 */ const slideUpAndFade = (0,_stitches_react__WEBPACK_IMPORTED_MODULE_2__.keyframes)({
    "0%": {
        opacity: 0,
        transform: "translateY(2px)"
    },
    "100%": {
        opacity: 1,
        transform: "translateY(0)"
    }
});
const slideRightAndFade = (0,_stitches_react__WEBPACK_IMPORTED_MODULE_2__.keyframes)({
    "0%": {
        opacity: 0,
        transform: "translateX(-2px)"
    },
    "100%": {
        opacity: 1,
        transform: "translateX(0)"
    }
});
const slideDownAndFade = (0,_stitches_react__WEBPACK_IMPORTED_MODULE_2__.keyframes)({
    "0%": {
        opacity: 0,
        transform: "translateY(-2px)"
    },
    "100%": {
        opacity: 1,
        transform: "translateY(0)"
    }
});
const slideLeftAndFade = (0,_stitches_react__WEBPACK_IMPORTED_MODULE_2__.keyframes)({
    "0%": {
        opacity: 0,
        transform: "translateX(2px)"
    },
    "100%": {
        opacity: 1,
        transform: "translateX(0)"
    }
});
const contentStyles = {
    minWidth: 220,
    backgroundColor: "rgb(27,30,32)",
    borderRadius: 6,
    padding: 5,
    boxShadow: "0px 10px 38px -10px rgba(22, 23, 24, 0.35), 0px 10px 20px -15px rgba(22, 23, 24, 0.2)",
    "@media (prefers-reduced-motion: no-preference)": {
        animationDuration: "400ms",
        animationTimingFunction: "cubic-bezier(0.16, 1, 0.3, 1)",
        willChange: "transform, opacity",
        '&[data-state="open"]': {
            '&[data-side="top"]': {
                animationName: slideDownAndFade
            },
            '&[data-side="right"]': {
                animationName: slideLeftAndFade
            },
            '&[data-side="bottom"]': {
                animationName: slideUpAndFade
            },
            '&[data-side="left"]': {
                animationName: slideRightAndFade
            }
        }
    }
};
const StyledContent = (0,_stitches_react__WEBPACK_IMPORTED_MODULE_2__.styled)(_radix_ui_react_dropdown_menu__WEBPACK_IMPORTED_MODULE_5__.Content, {
    ...contentStyles
});
const StyledArrow = (0,_stitches_react__WEBPACK_IMPORTED_MODULE_2__.styled)(_radix_ui_react_dropdown_menu__WEBPACK_IMPORTED_MODULE_5__.Arrow, {
    fill: "white"
});
function Content({ children , ...props }) {
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_radix_ui_react_dropdown_menu__WEBPACK_IMPORTED_MODULE_5__.Portal, {
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(StyledContent, {
            ...props,
            children: [
                children,
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(StyledArrow, {})
            ]
        })
    });
}
const StyledSubContent = (0,_stitches_react__WEBPACK_IMPORTED_MODULE_2__.styled)(_radix_ui_react_dropdown_menu__WEBPACK_IMPORTED_MODULE_5__.SubContent, {
    ...contentStyles
});
function SubContent(props) {
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_radix_ui_react_dropdown_menu__WEBPACK_IMPORTED_MODULE_5__.Portal, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(StyledSubContent, {
            ...props
        })
    });
}
const itemStyles = {
    all: "unset",
    fontSize: 13,
    lineHeight: 1,
    color: "#ffffff",
    borderRadius: 3,
    display: "flex",
    alignItems: "center",
    height: 25,
    padding: "0 5px",
    position: "relative",
    paddingLeft: 25,
    userSelect: "none",
    "&[data-disabled]": {
        color: _radix_ui_colors__WEBPACK_IMPORTED_MODULE_3__.mauve.mauve8,
        pointerEvents: "none"
    },
    "&[data-highlighted]": {
        backgroundColor: "#000000",
        color: "#ffffff"
    }
};
const StyledItem = (0,_stitches_react__WEBPACK_IMPORTED_MODULE_2__.styled)(_radix_ui_react_dropdown_menu__WEBPACK_IMPORTED_MODULE_5__.Item, {
    ...itemStyles
});
const StyledSubTrigger = (0,_stitches_react__WEBPACK_IMPORTED_MODULE_2__.styled)(_radix_ui_react_dropdown_menu__WEBPACK_IMPORTED_MODULE_5__.SubTrigger, {
    '&[data-state="open"]': {
        backgroundColor: "#000000",
        color: "#ffffff"
    },
    ...itemStyles
});
const StyledSeparator = (0,_stitches_react__WEBPACK_IMPORTED_MODULE_2__.styled)(_radix_ui_react_dropdown_menu__WEBPACK_IMPORTED_MODULE_5__.Separator, {
    height: 1,
    backgroundColor: "#b9b9b9",
    margin: 5
});
const DropdownMenu = _radix_ui_react_dropdown_menu__WEBPACK_IMPORTED_MODULE_5__.Root;
const DropdownMenuTrigger = _radix_ui_react_dropdown_menu__WEBPACK_IMPORTED_MODULE_5__.Trigger;
const DropdownMenuContent = Content;
const DropdownMenuItem = StyledItem;
const DropdownMenuSeparator = StyledSeparator;
const DropdownMenuSub = _radix_ui_react_dropdown_menu__WEBPACK_IMPORTED_MODULE_5__.Sub;
const DropdownMenuSubTrigger = StyledSubTrigger;
const DropdownMenuSubContent = SubContent;
const RightSlot = (0,_stitches_react__WEBPACK_IMPORTED_MODULE_2__.styled)("div", {
    marginLeft: "auto",
    paddingLeft: 20,
    color: _radix_ui_colors__WEBPACK_IMPORTED_MODULE_3__.mauve.mauve11,
    "[data-highlighted] > &": {
        color: "white"
    },
    "[data-disabled] &": {
        color: _radix_ui_colors__WEBPACK_IMPORTED_MODULE_3__.mauve.mauve8
    }
});
const IconButton = (0,_stitches_react__WEBPACK_IMPORTED_MODULE_2__.styled)("button", {
    all: "unset",
    fontFamily: "inherit",
    borderRadius: "100%",
    height: 24,
    width: 24,
    display: "inline-flex",
    alignItems: "center",
    justifyContent: "center",
    color: _radix_ui_colors__WEBPACK_IMPORTED_MODULE_3__.violet.violet11,
    backgroundColor: "white",
    boxShadow: `0 2px 10px ${_radix_ui_colors__WEBPACK_IMPORTED_MODULE_3__.blackA.blackA7}`,
    "&:hover": {
        backgroundColor: _radix_ui_colors__WEBPACK_IMPORTED_MODULE_3__.violet.violet3
    },
    "&:focus": {
        boxShadow: `0 0 0 2px black`
    }
});
function Navbar(props) {
    const [isOpen, setOpen] = react__WEBPACK_IMPORTED_MODULE_1___default().useState(false);
    let user = props.user;
    const ref = (0,react__WEBPACK_IMPORTED_MODULE_1__.useCallback)((node)=>{
        if (node !== null) {
            if (node.dataset.state === "open") {
                setOpen(true);
            } else {
                setOpen(false);
            }
        }
    }, []);
    // Logout
    const logout = async ()=>{
        let res = await (0,_lib_user__WEBPACK_IMPORTED_MODULE_8__/* .logoutUser */ .T)();
        if (res) next_router__WEBPACK_IMPORTED_MODULE_7___default().reload();
    };
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().navbarContainer),
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: `${(_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().navbarContentContainer)} ${(_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().leftNav)}`,
                    tabIndex: 0,
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: `${(_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().navbarContent)} ${(_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().leftNav)}`,
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_4__.GlobeIcon, {
                                className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().workSpaceIcon)
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().workSpaceName),
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().workSpaceNameTitleSmaller),
                                        children: [
                                            user.firstname,
                                            " ",
                                            user.lastname
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().workSpaceNameTitleSubtitle),
                                        children: user.work_email
                                    })
                                ]
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: `${(_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().smallIconContainer)} ${(_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().leftNav)}`,
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("svg", {
                                    className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().small),
                                    width: "8",
                                    height: "7",
                                    viewBox: "0 0 8 7",
                                    xmlns: "http://www.w3.org/2000/svg",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("path", {
                                        d: "M3.646 5.354l-3-3 .708-.708L4 4.293l2.646-2.647.708.708-3 3L4 5.707l-.354-.353z",
                                        fill: "#000",
                                        stroke: "none"
                                    })
                                })
                            })
                        ]
                    })
                })
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().navbarContentContainer),
                        tabIndex: 0,
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().navbarContent),
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_4__.BellIcon, {})
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: `${isOpen ? (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().navbarContentContainerActive) : ""} ${(_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().navbarContentContainer)} ${(_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().right)}`,
                        tabIndex: 1,
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(DropdownMenu, {
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(DropdownMenuTrigger, {
                                    asChild: true,
                                    ref: ref,
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: `${(_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().navbarContent)} ${(_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().buttonUser)}`,
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(IconButton, {
                                                children: user.work_email.charAt(0).toUpperCase()
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().smallIconContainer),
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("svg", {
                                                    className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().small),
                                                    width: "8",
                                                    height: "7",
                                                    viewBox: "0 0 8 7",
                                                    xmlns: "http://www.w3.org/2000/svg",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("path", {
                                                        d: "M3.646 5.354l-3-3 .708-.708L4 4.293l2.646-2.647.708.708-3 3L4 5.707l-.354-.353z",
                                                        fill: "#000",
                                                        stroke: "none"
                                                    })
                                                })
                                            })
                                        ]
                                    })
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(DropdownMenuContent, {
                                    sideOffset: 5,
                                    className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownMenu),
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownMenuUsername),
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownMenuUsernameIcon),
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(IconButton, {
                                                        "aria-label": "Customise options",
                                                        children: user.work_email.charAt(0).toUpperCase()
                                                    })
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownMenuUsernameEmail),
                                                    children: user.work_email
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(DropdownMenuItem, {
                                            className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownMenuItem),
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_4__.AvatarIcon, {
                                                    className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownIcon)
                                                }),
                                                " Internal Profile"
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(DropdownMenuItem, {
                                            className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownMenuItem),
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(evergreen_ui__WEBPACK_IMPORTED_MODULE_6__.LightningIcon, {
                                                    className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownIcon)
                                                }),
                                                " Plugins"
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(DropdownMenuItem, {
                                            className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownMenuItem),
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_4__.MixerVerticalIcon, {
                                                    className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownIcon)
                                                }),
                                                " Settings"
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(DropdownMenuSub, {
                                            children: [
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(DropdownMenuSubTrigger, {
                                                    className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownMenuItem),
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_4__.Half2Icon, {
                                                            className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownIcon)
                                                        }),
                                                        " Theme",
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(RightSlot, {
                                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_4__.ChevronRightIcon, {})
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(DropdownMenuSubContent, {
                                                    sideOffset: 2,
                                                    alignOffset: -5,
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(DropdownMenuItem, {
                                                            children: "Light"
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(DropdownMenuItem, {
                                                            children: "Dark"
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(DropdownMenuItem, {
                                                            children: "System Theme"
                                                        })
                                                    ]
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(DropdownMenuItem, {
                                            className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownMenuItem),
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_4__.PinBottomIcon, {
                                                    className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownIcon)
                                                }),
                                                " Get Desktop App"
                                            ]
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(DropdownMenuSeparator, {}),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(DropdownMenuItem, {
                                            className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownMenuItem),
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_4__.PlusIcon, {
                                                    className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownIcon)
                                                }),
                                                " Add account"
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(DropdownMenuItem, {
                                            className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownMenuItem),
                                            onClick: ()=>logout(),
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_4__.ExitIcon, {
                                                    className: (_styles_components_Navbar_module_scss__WEBPACK_IMPORTED_MODULE_9___default().dropdownIcon)
                                                }),
                                                "Logout"
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    })
                ]
            })
        ]
    });
}

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 7705:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ Sidebar)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles_components_Sidebar_module_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(8955);
/* harmony import */ var _styles_components_Sidebar_module_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_styles_components_Sidebar_module_scss__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5725);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(antd__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _lib_user__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5590);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2132);
/* harmony import */ var _radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_5__);







const { Sider  } = antd__WEBPACK_IMPORTED_MODULE_2__.Layout;
function Sidebar() {
    const [collapsed, setCollapsed] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(true);
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        setCollapsed(true);
    }, []);
    // Logout
    const logout = async ()=>{
        const userStatus = await (0,_lib_user__WEBPACK_IMPORTED_MODULE_3__/* .logoutUser */ .T)();
        if (userStatus) {
            next_router__WEBPACK_IMPORTED_MODULE_4___default().reload();
        } else {}
    };
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: (_styles_components_Sidebar_module_scss__WEBPACK_IMPORTED_MODULE_6___default().navDefault),
        id: "sidebar",
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: (_styles_components_Sidebar_module_scss__WEBPACK_IMPORTED_MODULE_6___default().scrollContainer),
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: (_styles_components_Sidebar_module_scss__WEBPACK_IMPORTED_MODULE_6___default().scrollContainerWithOverscroll),
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: (_styles_components_Sidebar_module_scss__WEBPACK_IMPORTED_MODULE_6___default().innerScrollContainer),
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {}),
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: (_styles_components_Sidebar_module_scss__WEBPACK_IMPORTED_MODULE_6___default().navContent),
                            children: [
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: (_styles_components_Sidebar_module_scss__WEBPACK_IMPORTED_MODULE_6___default().sidebarSection),
                                    onClick: ()=>next_router__WEBPACK_IMPORTED_MODULE_4___default().push("/", undefined, {
                                            shallow: true
                                        }),
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: (_styles_components_Sidebar_module_scss__WEBPACK_IMPORTED_MODULE_6___default().sidebarIconContainer),
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_5__.HomeIcon, {
                                                className: (_styles_components_Sidebar_module_scss__WEBPACK_IMPORTED_MODULE_6___default().sidebarIcon)
                                            })
                                        }),
                                        " Home"
                                    ]
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: (_styles_components_Sidebar_module_scss__WEBPACK_IMPORTED_MODULE_6___default().sidebarSection),
                                    onClick: ()=>next_router__WEBPACK_IMPORTED_MODULE_4___default().push("/applications", undefined, {
                                            shallow: true
                                        }),
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: (_styles_components_Sidebar_module_scss__WEBPACK_IMPORTED_MODULE_6___default().sidebarIconContainer),
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_5__.MixIcon, {
                                                className: (_styles_components_Sidebar_module_scss__WEBPACK_IMPORTED_MODULE_6___default().sidebarIcon)
                                            })
                                        }),
                                        " Applications"
                                    ]
                                })
                            ]
                        })
                    ]
                })
            })
        })
    });
}


/***/ }),

/***/ 7590:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ ToolBar)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles_components_Toolbar_module_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1923);
/* harmony import */ var _styles_components_Toolbar_module_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_styles_components_Toolbar_module_scss__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2132);
/* harmony import */ var _radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _lib_hooks__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8055);





function ToolBar(props) {
    const [isMenuClosed, setMenuClosed] = react__WEBPACK_IMPORTED_MODULE_1___default().useState(true);
    const showMenu = ()=>{
        let element = document.getElementById("sidebar");
        if (element) {
            if (element.style.left === "") {
                element.style.left = "0";
                setMenuClosed(false);
            } else {
                element.style.left = "";
                setMenuClosed(true);
            }
        }
    };
    const { user  } = (0,_lib_hooks__WEBPACK_IMPORTED_MODULE_3__/* .useUser */ .a)({});
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{}, [
        user
    ]);
    // Loading state
    if (user.isLoggedIn === undefined) {
        return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {});
    }
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: (_styles_components_Toolbar_module_scss__WEBPACK_IMPORTED_MODULE_4___default().toolbarSpacerContainerMobile),
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: (_styles_components_Toolbar_module_scss__WEBPACK_IMPORTED_MODULE_4___default().toolbarMobileLeft),
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: (_styles_components_Toolbar_module_scss__WEBPACK_IMPORTED_MODULE_4___default().toolbarMobileButtonMenu),
                            onClick: ()=>showMenu(),
                            children: isMenuClosed ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_2__.RowsIcon, {}) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_radix_ui_react_icons__WEBPACK_IMPORTED_MODULE_2__.Cross1Icon, {})
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: (_styles_components_Toolbar_module_scss__WEBPACK_IMPORTED_MODULE_4___default().toolbarMobileTitle),
                            children: props.title
                        })
                    ]
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: (_styles_components_Toolbar_module_scss__WEBPACK_IMPORTED_MODULE_4___default().toolbarSpacerContainer),
                children: props.title
            })
        ]
    });
}


/***/ }),

/***/ 7857:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ LoadingState)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);
// EXTERNAL MODULE: ./lib/hooks.ts
var hooks = __webpack_require__(8055);
// EXTERNAL MODULE: ./styles/pages/Homepage.module.scss
var Homepage_module = __webpack_require__(2477);
var Homepage_module_default = /*#__PURE__*/__webpack_require__.n(Homepage_module);
// EXTERNAL MODULE: external "@react-spring/parallax"
var parallax_ = __webpack_require__(7436);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);
// EXTERNAL MODULE: external "evergreen-ui"
var external_evergreen_ui_ = __webpack_require__(4371);
// EXTERNAL MODULE: external "@ant-design/icons"
var icons_ = __webpack_require__(7066);
// EXTERNAL MODULE: external "antd"
var external_antd_ = __webpack_require__(5725);
;// CONCATENATED MODULE: ./components/notLoggedin/index.tsx








const Index = ()=>{
    const parallax = (0,external_react_.useRef)(null);
    const url = (name, wrap = false)=>`${wrap ? "url(" : ""}https://awv3node-homepage.surge.sh/build/assets/${name}.svg${wrap ? ")" : ""}`;
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_antd_.Layout, {
        className: (Homepage_module_default()).layout,
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(parallax_.Parallax, {
                ref: parallax,
                pages: 1.2,
                className: (Homepage_module_default()).parallax,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "row center",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "cl--100",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: (Homepage_module_default()).navbar,
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: (Homepage_module_default()).logo,
                                        children: "HESSIC"
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: (Homepage_module_default()).navbarDesktop,
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: (Homepage_module_default()).navbarDesktopComponent,
                                                children: "Solutions"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: (Homepage_module_default()).navbarDesktopComponent,
                                                children: "Support"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: (Homepage_module_default()).navbarDesktopComponent,
                                                children: "Resources"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: (Homepage_module_default()).navbarDesktopComponent,
                                                children: "Company"
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: (Homepage_module_default()).navbarDesktopComponentsRight,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: (Homepage_module_default()).navbarDesktopComponent,
                                                        children: "Request a demo"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: (Homepage_module_default()).navbarDesktopComponent,
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                            href: "/login",
                                                            children: "Sign up"
                                                        })
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: (Homepage_module_default()).navbarMobile,
                                        children: /*#__PURE__*/ jsx_runtime_.jsx(external_evergreen_ui_.Popover, {
                                            position: external_evergreen_ui_.Position.BOTTOM_LEFT,
                                            content: /*#__PURE__*/ jsx_runtime_.jsx(external_evergreen_ui_.Menu, {
                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)(external_evergreen_ui_.Menu.Group, {
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx(external_evergreen_ui_.Menu.Item, {
                                                            onSelect: ()=>external_evergreen_ui_.toaster.notify("Share"),
                                                            children: "Solutions"
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx(external_evergreen_ui_.Menu.Item, {
                                                            onSelect: ()=>external_evergreen_ui_.toaster.notify("Move"),
                                                            children: "Support"
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx(external_evergreen_ui_.Menu.Item, {
                                                            onSelect: ()=>external_evergreen_ui_.toaster.notify("Share"),
                                                            children: "Resources"
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx(external_evergreen_ui_.Menu.Item, {
                                                            onSelect: ()=>external_evergreen_ui_.toaster.notify("Move"),
                                                            children: "Company"
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx(external_evergreen_ui_.Menu.Item, {
                                                            onSelect: ()=>external_evergreen_ui_.toaster.notify("Share"),
                                                            children: "Request a demo"
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx(external_evergreen_ui_.Menu.Item, {
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                                                href: "/hessic_front-end/pages/login",
                                                                children: "Sign up"
                                                            })
                                                        })
                                                    ]
                                                })
                                            }),
                                            children: /*#__PURE__*/ jsx_runtime_.jsx(icons_.MenuOutlined, {})
                                        })
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(parallax_.ParallaxLayer, {
                        className: (Homepage_module_default()).parallaxBackground,
                        offset: 1,
                        speed: 1,
                        style: {
                            backgroundColor: "#000000"
                        }
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(parallax_.ParallaxLayer, {
                        offset: 0,
                        speed: 0,
                        factor: 3,
                        style: {
                            backgroundImage: url("stars", true),
                            backgroundSize: "cover"
                        }
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(parallax_.ParallaxLayer, {
                        offset: 0.2,
                        speed: 0.1,
                        onClick: ()=>parallax.current.scrollTo(1),
                        className: (Homepage_module_default()).parallaxText,
                        style: {
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center"
                        },
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: (Homepage_module_default()).background,
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: (Homepage_module_default()).title,
                                    children: "Data Analytics."
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: (Homepage_module_default()).subTitle,
                                    children: "Unified. Democratized."
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: (Homepage_module_default()).description,
                                    children: "State-of-the art technologies, and advanced analytics tools designed to meet every industry's needs."
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(parallax_.ParallaxLayer, {
                        offset: 0.65,
                        speed: 0.5,
                        style: {
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center"
                        },
                        // className={IndexStyles.parallaxComing}
                        onClick: ()=>parallax.current.scrollTo(0),
                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: (Homepage_module_default()).comingSoon,
                            children: "Coming in 2023."
                        })
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: `row center ${(Homepage_module_default()).copyright}`,
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: `cl--70 flex ${(Homepage_module_default()).copyrightRow}`,
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("span", {
                            className: (Homepage_module_default()).copyrightText,
                            children: "Copyright \xa9 2022 HESSIC. All rights reserved. "
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("span", {
                            className: (Homepage_module_default()).termsOfUse,
                            children: "Terms of Use & Privacy Policy"
                        })
                    ]
                })
            })
        ]
    });
};
/* harmony default export */ const notLoggedin = (Index);

// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
var router_default = /*#__PURE__*/__webpack_require__.n(router_);
// EXTERNAL MODULE: ./lib/user.tsx
var user = __webpack_require__(5590);
// EXTERNAL MODULE: ./styles/pages/LoginSignup.module.scss
var LoginSignup_module = __webpack_require__(5410);
var LoginSignup_module_default = /*#__PURE__*/__webpack_require__.n(LoginSignup_module);
;// CONCATENATED MODULE: ./components/notAuthorized-loggedin-verified/index.tsx






const notAuthorized_loggedin_verified_Index = ()=>{
    // Logout
    const logout = async ()=>{
        let res = await (0,user/* logoutUser */.T)();
        if (res) router_default().reload();
    };
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        className: (LoginSignup_module_default()).container,
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: `${(LoginSignup_module_default()).header}`,
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: `cl--90 ${(LoginSignup_module_default()).headerContent}`,
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                            href: "/hessic_front-end/pages",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                className: (LoginSignup_module_default()).logo,
                                children: "HESSIC"
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("span", {
                            children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                href: "/",
                                onClick: logout,
                                children: "Logout"
                            })
                        })
                    ]
                })
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: (LoginSignup_module_default()).card,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: (LoginSignup_module_default()).cardTitle,
                        children: "Private Alpha"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                        className: (LoginSignup_module_default()).cardSubtitle,
                        children: "Hey! Thanks for your interest in HESSIC."
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("p", {
                        children: "HESSIC is currently in closed Alpha."
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                        children: [
                            "You can request access here if you are a company or an institutional investor: ",
                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                href: "mailto: hello@hessic.com",
                                children: "hello@hessic.com"
                            })
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("p", {
                        children: "In the meanwhile, you can enter your email address to subscribe for new updates:"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("link", {
                        href: "//cdn-images.mailchimp.com/embedcode/classic-10_7_dtp.css",
                        rel: "stylesheet",
                        type: "text/css"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("style", {
                        type: "text/css",
                        dangerouslySetInnerHTML: {
                            __html: "\n	#mc_embed_signup{background:#fff; clear:left; padding-right: 60px; font:14px Helvetica,Arial,sans-serif; }\n	/* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.\n	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */\n"
                        }
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        id: "mc_embed_signup",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("form", {
                            action: "https://hessic.us12.list-manage.com/subscribe/post?u=feb993b3ff6b0f24baa338853&id=3c9adc30c2",
                            method: "post",
                            id: "mc-embedded-subscribe-form",
                            name: "mc-embedded-subscribe-form",
                            className: "validate",
                            target: "_blank",
                            noValidate: true,
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                id: "mc_embed_signup_scroll",
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "indicates-required",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                className: "asterisk",
                                                children: "*"
                                            }),
                                            " indicates required"
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "mc-field-group",
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("label", {
                                                htmlFor: "mce-EMAIL",
                                                children: [
                                                    "Email Address  ",
                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        className: "asterisk",
                                                        children: "*"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                type: "email",
                                                name: "EMAIL",
                                                className: "required email",
                                                id: "mce-EMAIL"
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        id: "mce-responses",
                                        className: "clear foot",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "response",
                                                id: "mce-error-response",
                                                style: {
                                                    display: "none"
                                                }
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "response",
                                                id: "mce-success-response",
                                                style: {
                                                    display: "none"
                                                }
                                            })
                                        ]
                                    }),
                                    "    ",
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        style: {
                                            position: "absolute",
                                            left: "-5000px"
                                        },
                                        "aria-hidden": "true",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                            type: "text",
                                            name: "b_feb993b3ff6b0f24baa338853_3c9adc30c2",
                                            tabIndex: -1
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: "optionalParent",
                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: "clear foot",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                    type: "submit",
                                                    defaultValue: "Subscribe",
                                                    name: "subscribe",
                                                    id: "mc-embedded-subscribe",
                                                    className: "button"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                    className: "brandingLogo",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                        href: "http://eepurl.com/h5YsQ9",
                                                        title: "Mailchimp - email marketing made easy and fun",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "https://eep.io/mc-cdn-images/template_images/branding_logo_text_dark_dtp.svg"
                                                        })
                                                    })
                                                })
                                            ]
                                        })
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        id: "vertical_space_20"
                    })
                ]
            })
        ]
    });
};
/* harmony default export */ const notAuthorized_loggedin_verified = (notAuthorized_loggedin_verified_Index);

;// CONCATENATED MODULE: ./components/loggedin-notVerified/index.tsx






const HomepageUserAuthenticatedVerified = ()=>{
    // Logout
    const logout = async ()=>{
        const userStatus = await (0,user/* logoutUser */.T)();
        if (userStatus) {
            router_default().reload();
        } else {}
    };
    // Request another verification
    let requestVerification = ()=>{
        fetch("http://localhost:4000" + "/api/verification/resend", {
            credentials: "include" // send cookies to url
        }).then((r)=>r.json()).then((data)=>{
            console.log(data.status);
            if (data.status === 200) {
                external_antd_.message.success("New verification code sent. Please check your inbox.");
            } else {
                external_antd_.message.error("Error while requesting a new verification code");
            }
        });
    };
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        className: (LoginSignup_module_default()).container,
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: `${(LoginSignup_module_default()).header}`,
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: `cl--90 ${(LoginSignup_module_default()).headerContent}`,
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                            href: "/hessic_front-end/pages",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                className: (LoginSignup_module_default()).logo,
                                children: "HESSIC"
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("span", {
                            children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                href: "/",
                                onClick: logout,
                                children: "Logout"
                            })
                        })
                    ]
                })
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: (LoginSignup_module_default()).card,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                        className: (LoginSignup_module_default()).LargeTitle,
                        children: "Account not verified"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                    "Please check your email for verification link, or",
                    /*#__PURE__*/ jsx_runtime_.jsx("a", {
                        onClick: requestVerification,
                        children: " request a new verification"
                    })
                ]
            })
        ]
    });
};
/* harmony default export */ const loggedin_notVerified = (HomepageUserAuthenticatedVerified);

;// CONCATENATED MODULE: ./components/loadingState.tsx






function LoadingState(props) {
    const { user  } = (0,hooks/* useUser */.a)({});
    (0,external_react_.useEffect)(()=>{}, [
        user
    ]);
    // Loading state
    if (user.isLoggedIn === undefined) {
        return /*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {});
    } else if (user.isLoggedIn && user.isAdmin) {
        return /*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
            children: /*#__PURE__*/ external_react_default().cloneElement(props.children, {
                user: user
            })
        });
    } else if (user.isLoggedIn && user.isVerified && !user.isAdmin) {
        return /*#__PURE__*/ jsx_runtime_.jsx(notAuthorized_loggedin_verified, {});
    } else if (user.isLoggedIn && !user.isVerified) {
        return /*#__PURE__*/ jsx_runtime_.jsx(loggedin_notVerified, {});
    } else {
        return /*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
            children: /*#__PURE__*/ jsx_runtime_.jsx(notLoggedin, {})
        });
    }
}


/***/ }),

/***/ 5590:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "T": () => (/* binding */ logoutUser)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

/**
 * Logs out user
 *
 * @remarks
 * This function logs out the user by removing the token cookie
 *
 * @returns res - Object to send back the desired HTTP response
 *
 * @beta
 */ async function logoutUser() {
    const res = await fetch("http://localhost:4000" + "/api/logout", {
        credentials: "include" // send cookies to url
    });
    return res;
}


/***/ })

};
;