"use strict";
exports.id = 55;
exports.ids = [55];
exports.modules = {

/***/ 8055:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "a": () => (/* binding */ useUser)
/* harmony export */ });
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


/**
 * Handles front-end user authentication.
 *
 * @param redirectTo - The path to follow given redirectIfFound and authentication status
 * @param redirectIfFound - If redirectIfFound is set to false and the user is not logged in, then follow redirectTo
 *                          If redirectIfFound is set to true and the user is logged in, then follow redirectTo
 *
 * @returns User information if user is authenticated, otherwise null
 *
 * @beta
 */ function useUser({ redirectTo ="" , redirectIfFound =false  } = {}) {
    let router = (0,next_router__WEBPACK_IMPORTED_MODULE_0__.useRouter)();
    const [user, setUser] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)({
        isLoggedIn: undefined,
        isAdmin: undefined,
        isVerified: undefined
    });
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        /**
         * HTTP request given an url
         *
         * @param url - The url to fetch
         *
         * @returns User info if authenticated, otherwise null
         *
         * @beta
         */ fetch("http://localhost:4000" + "/api/user", {
            credentials: "include" // send cookies to url
        }).then((r)=>r.json()).then((data)=>{
            setUser(data?.user);
            // if no redirect needed, just return (example: already on /dashboard)
            // if user data not yet there (fetch in progress, logged in or not) then don't do anything yet
            if (!redirectTo || !data?.user) return;
            if (redirectTo && !redirectIfFound && !data?.user?.isLoggedIn || // if redirectTo is set, redirect if the user was not found.
            redirectIfFound && data?.user?.isLoggedIn // if redirectIfFound is also set, redirect if the user was found
            ) {
                next_router__WEBPACK_IMPORTED_MODULE_0___default().push(redirectTo, undefined, {
                    shallow: false
                });
            }
        });
    }, [
        router.pathname,
        redirectTo,
        redirectIfFound
    ]);
    return {
        user
    };
}


/***/ })

};
;