exports.id = 410;
exports.ids = [410];
exports.modules = {

/***/ 5410:
/***/ ((module) => {

// Exports
module.exports = {
	"LargeTitle": "LoginSignup_LargeTitle__Pnn2j",
	"container": "LoginSignup_container__TwEb_",
	"header": "LoginSignup_header__S6ooB",
	"headerContent": "LoginSignup_headerContent__nRq_3",
	"logo": "LoginSignup_logo__Q34wY",
	"card": "LoginSignup_card__DdAjP",
	"cardTitle": "LoginSignup_cardTitle___1Sry",
	"cardSubtitle": "LoginSignup_cardSubtitle__sedIz",
	"formItem": "LoginSignup_formItem__AD6QC",
	"continueButton": "LoginSignup_continueButton__3o2QN",
	"checkBox": "LoginSignup_checkBox__Pi5tY",
	"footer": "LoginSignup_footer__c0omx",
	"footerContainer": "LoginSignup_footerContainer__DlcKq",
	"footerItem": "LoginSignup_footerItem__4N_OY"
};


/***/ })

};
;