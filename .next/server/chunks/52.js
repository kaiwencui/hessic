exports.id = 52;
exports.ids = [52];
exports.modules = {

/***/ 4052:
/***/ ((module) => {

// Exports
module.exports = {
	"pageViewContent": "AppBrowse_pageViewContent__GFefh",
	"boxContainer": "AppBrowse_boxContainer__4xtrF",
	"contentContainer": "AppBrowse_contentContainer__XVZ4n",
	"custom": "AppBrowse_custom__tqvXs",
	"gradientBox": "AppBrowse_gradientBox__zHYt_",
	"gradientBox2": "AppBrowse_gradientBox2__L2f2i",
	"gradientBox3": "AppBrowse_gradientBox3__hcDWf",
	"gradientBoxLeftSide": "AppBrowse_gradientBoxLeftSide__yJ7yV",
	"gradientBoxLeftSideTextSmall": "AppBrowse_gradientBoxLeftSideTextSmall__Z3M2g",
	"gradientBoxLeftSideTextBig": "AppBrowse_gradientBoxLeftSideTextBig__fCFhG",
	"subheaderText": "AppBrowse_subheaderText__pTOZh",
	"headerTitle": "AppBrowse_headerTitle__UzeGO",
	"headerTitle2": "AppBrowse_headerTitle2__ggUAr",
	"seeAllLink": "AppBrowse_seeAllLink__4jX_M",
	"capitalizedSubText": "AppBrowse_capitalizedSubText__zhjPz",
	"capitalizedSubText2": "AppBrowse_capitalizedSubText2__ppyts",
	"scrollableDiv": "AppBrowse_scrollableDiv__PEaZ2",
	"AppHeader": "AppBrowse_AppHeader__IGUh2",
	"AppHeaderContent": "AppBrowse_AppHeaderContent__TUz3U",
	"AppHeaderLeft": "AppBrowse_AppHeaderLeft__EHVRo",
	"AppHeaderRight": "AppBrowse_AppHeaderRight__O2w58",
	"AppHeaderRightButtonContainer": "AppBrowse_AppHeaderRightButtonContainer__zYmUo",
	"AppHeaderRightButton": "AppBrowse_AppHeaderRightButton__1O5l_",
	"AppHeaderLeftTitle": "AppBrowse_AppHeaderLeftTitle__oaX5M",
	"AppHeaderLeftSubTitle": "AppBrowse_AppHeaderLeftSubTitle__VLocG",
	"AppHeaderLeftSubTitleDescription": "AppBrowse_AppHeaderLeftSubTitleDescription__HBiN9",
	"AppHeaderMobile": "AppBrowse_AppHeaderMobile__HecQ9",
	"AppHeaderButtonMobile": "AppBrowse_AppHeaderButtonMobile__9DWu0",
	"iconButton": "AppBrowse_iconButton__Qlewc",
	"imageCover": "AppBrowse_imageCover__3rgAI",
	"AppDetailsContainer": "AppBrowse_AppDetailsContainer__VyeXa",
	"metadataContainer": "AppBrowse_metadataContainer__M6Yfn",
	"metadataContainerLeft": "AppBrowse_metadataContainerLeft__IEiHQ",
	"metadataContainerRight": "AppBrowse_metadataContainerRight__v9UGm",
	"metadataContainerRightSection": "AppBrowse_metadataContainerRightSection__j_QBC",
	"metadataContainerLeftVersion": "AppBrowse_metadataContainerLeftVersion__92GiB",
	"metadataLeftVersion": "AppBrowse_metadataLeftVersion__WkIqH",
	"metadataLeftTitle": "AppBrowse_metadataLeftTitle__Kaz4p",
	"metadataLeftSubTitle": "AppBrowse_metadataLeftSubTitle__bJeLM"
};


/***/ })

};
;