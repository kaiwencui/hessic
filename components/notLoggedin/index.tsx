import IndexStyles from "../../styles/pages/Homepage.module.scss";
import {IParallax, Parallax, ParallaxLayer} from "@react-spring/parallax";
import Link from "next/link";
import {Menu, Popover, Position, toaster} from "evergreen-ui";
import {MenuOutlined} from "@ant-design/icons";
import {Layout} from "antd";
import React, {useRef} from "react";

const Index = () => {

    const parallax = useRef<IParallax>(null!)
    const url = (name: string, wrap = false) =>
        `${wrap ? 'url(' : ''}https://awv3node-homepage.surge.sh/build/assets/${name}.svg${wrap ? ')' : ''}`

    return(

        <Layout className={IndexStyles.layout}>
            <Parallax ref={parallax} pages={1.2} className={IndexStyles.parallax}>
                {/* Sidebar */}
                <div className="row center">
                    <div className="cl--100">
                        <div className={IndexStyles.navbar}>
                            <div className={IndexStyles.logo}>HESSIC</div>
                            <div className={IndexStyles.navbarDesktop}>
                                <div className={IndexStyles.navbarDesktopComponent}>Solutions</div>
                                <div className={IndexStyles.navbarDesktopComponent}>Support</div>
                                <div className={IndexStyles.navbarDesktopComponent}>Resources</div>
                                <div className={IndexStyles.navbarDesktopComponent}>Company</div>
                                <div className={IndexStyles.navbarDesktopComponentsRight}>
                                    <div className={IndexStyles.navbarDesktopComponent}>Request a demo</div>
                                    <div className={IndexStyles.navbarDesktopComponent}>
                                        <Link href="/login">
                                            Sign up
                                        </Link>
                                    </div>
                                </div>
                            </div>
                            <div className={IndexStyles.navbarMobile}>
                                <Popover
                                    position={Position.BOTTOM_LEFT}
                                    content={
                                        <Menu>
                                            <Menu.Group>
                                                <Menu.Item
                                                    onSelect={() => toaster.notify('Share')}>Solutions</Menu.Item>
                                                <Menu.Item onSelect={() => toaster.notify('Move')}>Support</Menu.Item>
                                                <Menu.Item
                                                    onSelect={() => toaster.notify('Share')}>Resources</Menu.Item>
                                                <Menu.Item onSelect={() => toaster.notify('Move')}>Company</Menu.Item>
                                                <Menu.Item onSelect={() => toaster.notify('Share')}>Request a
                                                    demo</Menu.Item>
                                                <Menu.Item>
                                                    <Link href="/hessic_front-end/pages/login">
                                                        Sign up
                                                    </Link>
                                                </Menu.Item>
                                            </Menu.Group>
                                        </Menu>
                                    }
                                >
                                    <MenuOutlined/>
                                </Popover>
                            </div>
                        </div>
                    </div>
                </div>
                <ParallaxLayer className={IndexStyles.parallaxBackground} offset={1} speed={1} style={{backgroundColor: '#000000'}}/>

                <ParallaxLayer
                    offset={0}
                    speed={0}
                    factor={3}
                    style={{
                        backgroundImage: url('stars', true),
                        backgroundSize: 'cover',
                    }}
                />

                <ParallaxLayer
                    offset={0.2}
                    speed={0.1}
                    onClick={() => parallax.current.scrollTo(1)}
                    className={IndexStyles.parallaxText}
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                    <div className={IndexStyles.background}>
                        <div className={IndexStyles.title}>Data Analytics.</div>
                        <div className={IndexStyles.subTitle}>Unified. Democratized.</div>
                        <div className={IndexStyles.description}>
                            State-of-the art technologies, and advanced analytics tools designed to meet every
                            industry&apos;s needs.
                        </div>
                    </div>
                </ParallaxLayer>

                {/*<ParallaxLayer*/}
                {/*    offset={0.65}*/}
                {/*    speed={0.3}*/}
                {/*    onClick={() => parallax.current.scrollTo(2)}*/}
                {/*    className={IndexStyles.parallaxImage}*/}
                {/*    style={{*/}
                {/*        display: 'flex',*/}
                {/*        alignItems: 'center',*/}
                {/*        justifyContent: 'center'*/}
                {/*    }}>*/}
                {/*    <img src="/home/background-2.png" className={IndexStyles.backgroundImage2}></img>*/}
                {/*</ParallaxLayer>*/}

                <ParallaxLayer
                    offset={0.65}
                    speed={0.5}
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                    // className={IndexStyles.parallaxComing}
                    onClick={() => parallax.current.scrollTo(0)}>
                    <div className={IndexStyles.comingSoon}>Coming in 2023.</div>
                </ParallaxLayer>
            </Parallax>

            <div className={`row center ${IndexStyles.copyright}`}>
                <div className={`cl--70 flex ${IndexStyles.copyrightRow}`}>
                    <span className={IndexStyles.copyrightText}>Copyright © 2022 HESSIC. All rights reserved. </span>
                    <span className={IndexStyles.termsOfUse}>Terms of Use & Privacy Policy</span>
                </div>
            </div>
        </Layout>

    )
}

export default Index