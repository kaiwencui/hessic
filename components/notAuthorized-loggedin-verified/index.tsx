import Router from "next/router";
import {logoutUser} from "../../lib/user";
import Link from "next/link";
import React from "react";
import LoginRegisterStyles from "../../styles/pages/LoginSignup.module.scss";

const Index = () => {

    // Logout
    const logout = async () => {
        let res = await logoutUser();
        if (res)
            Router.reload()
    };


    return (
        <div className={LoginRegisterStyles.container}>

            {/* Header */}
            <div className={`${LoginRegisterStyles.header}`}>
                <div className={`cl--90 ${LoginRegisterStyles.headerContent}`}>
                    <Link href="/hessic_front-end/pages"><span className={LoginRegisterStyles.logo}>HESSIC</span></Link>
                    <span>
                        <Link href="/" onClick={logout}>
                            Logout
                        </Link>
                    </span>
                </div>
            </div>

            {/* Card */}
            <div className={LoginRegisterStyles.card}>
                <div className={LoginRegisterStyles.cardTitle}>Private Alpha</div>
                <h4 className={LoginRegisterStyles.cardSubtitle}>Hey! Thanks for your interest in HESSIC.</h4>
                <p>HESSIC is currently in closed Alpha.</p>
                <p>You can request access here if you are a company or an institutional investor: <a href="mailto: hello@hessic.com">hello@hessic.com</a></p>
                <p>In the meanwhile, you can enter your email address to subscribe for new updates:</p>

                {/* Begin Mailchimp Signup Form */}
                <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7_dtp.css" rel="stylesheet" type="text/css" />
                <style type="text/css" dangerouslySetInnerHTML={{__html: "\n\t#mc_embed_signup{background:#fff; clear:left; padding-right: 60px; font:14px Helvetica,Arial,sans-serif; }\n\t/* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.\n\t   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */\n" }} />
                <div id="mc_embed_signup">
                    <form action="https://hessic.us12.list-manage.com/subscribe/post?u=feb993b3ff6b0f24baa338853&id=3c9adc30c2" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" className="validate" target="_blank" noValidate>
                        <div id="mc_embed_signup_scroll">
                            <div className="indicates-required"><span className="asterisk">*</span> indicates required</div>
                            <div className="mc-field-group">
                                <label htmlFor="mce-EMAIL">Email Address  <span className="asterisk">*</span>
                                </label>
                                <input type="email" name="EMAIL" className="required email" id="mce-EMAIL" />
                            </div>
                            <div id="mce-responses" className="clear foot">
                                <div className="response" id="mce-error-response" style={{display: 'none'}} />
                                <div className="response" id="mce-success-response" style={{display: 'none'}} />
                            </div>    {/* real people should not fill this in and expect good things - do not remove this or risk form bot signups*/}
                            <div style={{position: 'absolute', left: '-5000px'}} aria-hidden="true"><input type="text" name="b_feb993b3ff6b0f24baa338853_3c9adc30c2" tabIndex={-1} /></div>
                            <div className="optionalParent">
                                <div className="clear foot">
                                    <input type="submit" defaultValue="Subscribe" name="subscribe" id="mc-embedded-subscribe" className="button" />
                                    <p className="brandingLogo"><a href="http://eepurl.com/h5YsQ9" title="Mailchimp - email marketing made easy and fun"><img src="https://eep.io/mc-cdn-images/template_images/branding_logo_text_dark_dtp.svg" /></a></p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="vertical_space_20"/>

            </div>

        </div>
    )
}

export default Index
