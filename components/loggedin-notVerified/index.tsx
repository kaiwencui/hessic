import Router from "next/router";
import {logoutUser} from "../../lib/user";
import LoginRegisterStyles from "../../styles/pages/LoginSignup.module.scss";
import Link from "next/link";
import {message} from "antd";

const HomepageUserAuthenticatedVerified = () => {

    // Logout
    const logout = async () => {
        const userStatus = await logoutUser();
        if (userStatus) {
            Router.reload();
        } else {
        }
    };

    // Request another verification
    let requestVerification = () => {

        fetch(process.env.REACT_APP_BACKEND_URL + "/api/verification/resend", {
            credentials: 'include' // send cookies to url
        })
            .then((r) => r.json())
            .then((data) => {
                console.log(data.status)
                if (data.status === 200) {
                    message.success("New verification code sent. Please check your inbox.");
                } else {
                    message.error("Error while requesting a new verification code")
                }
            })
    }


    return (
        <div className={LoginRegisterStyles.container}>

            {/* Header */}
            <div className={`${LoginRegisterStyles.header}`}>
                <div className={`cl--90 ${LoginRegisterStyles.headerContent}`}>
                    <Link href="/hessic_front-end/pages"><span className={LoginRegisterStyles.logo}>HESSIC</span></Link>
                    <span>
                        <Link href="/" onClick={logout}>
                            Logout
                        </Link>
                    </span>

                </div>
            </div>

            {/* Card */}
            <div className={LoginRegisterStyles.card}>
                <span className={LoginRegisterStyles.LargeTitle}>Account not verified</span>
                <br></br><br></br>
                Please check your email for verification link, or
                <a onClick={requestVerification}> request a new verification</a>
            </div>
        </div>

    )
}

export default HomepageUserAuthenticatedVerified
