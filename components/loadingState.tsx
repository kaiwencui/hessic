import React, {useEffect} from "react";
import {useUser} from "../lib/hooks";
import {default as NotLoggedIn} from "../components/notLoggedin/index";
import {default as NotAuthorizedLoggedinVerified} from "../components/notAuthorized-loggedin-verified/index";
import {default as LoggedinNotVerified} from "../components/loggedin-notVerified/index";

export default function LoadingState(props:any):any {

    const { user } = useUser({})

    useEffect(() => {
    }, [user])

    // Loading state
    if (user.isLoggedIn === undefined) {
        return <></>
    }

    // User is logged in, and verified, and is authorized
    else if (user.isLoggedIn && user.isAdmin) {
        return <>{React.cloneElement(props.children, {user: user})}</>
    }

    // User is logged in, and verified, but it's not an authorized
    else if (user.isLoggedIn && user.isVerified && !user.isAdmin) {
        return <NotAuthorizedLoggedinVerified/>
    }

    // User is logged in, but not verified
    else if (user.isLoggedIn && !user.isVerified) {
        return <LoggedinNotVerified/>
    }

    else {
        return (
            <><NotLoggedIn/></>
        )
    }
}