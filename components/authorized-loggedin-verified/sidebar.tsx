import React, {useEffect, useState} from 'react';
import style from "../../styles/components/Sidebar.module.scss";
import {Layout} from 'antd';
import Link from "next/link";
import {logoutUser} from "../../lib/user";
import Router from "next/router";
import {HomeIcon, MixIcon} from "@radix-ui/react-icons";
import {Route} from "react-router";
const {Sider} = Layout;


export default function Sidebar() {

    const [collapsed, setCollapsed] = useState(true);

    useEffect(() => {
        setCollapsed(true);
    }, []);

    // Logout
    const logout = async () => {
        const userStatus = await logoutUser();
        if (userStatus) {
            Router.reload();
        } else {
        }
    };


    return (
        <div className={style.navDefault} id={"sidebar"}>
            <div className={style.scrollContainer}>
                <div className={style.scrollContainerWithOverscroll}>
                    <div className={style.innerScrollContainer}>
                        <div></div>
                        <div className={style.navContent}>
                            <div className={style.sidebarSection} onClick={()=> Router.push("/", undefined, { shallow: true })}>
                                <div className={style.sidebarIconContainer}><HomeIcon className={style.sidebarIcon}/></div> Home
                            </div>
                            <div className={style.sidebarSection} onClick={()=> Router.push("/applications", undefined, { shallow: true })}>
                                <div className={style.sidebarIconContainer}><MixIcon className={style.sidebarIcon}/></div> Applications
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        //         <Menu.Item key="3" icon={<CopyOutlined/>}>
        //           <Link href={"/"} className="nav-text">Cases</Link>
        //         </Menu.Item>
        //         <Menu.Item key="4" icon={<UserOutlined/>}>
        //             <Link href={"/settings/profile"} className="nav-text">Profile</Link>
        //         </Menu.Item>
        //         <Menu.Item key="5" icon={<LogoutOutlined/>}>
        //             <a onClick={logout} className="nav-text">Logout</a>
        //         </Menu.Item>
        //     </Menu>
        // </Sider>
    );
}
