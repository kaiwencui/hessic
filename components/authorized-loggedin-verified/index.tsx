import React from 'react';
import HomeStyle from "../../styles/pages/HomepageUserAuthenticatedAdmin.module.scss"
import {StatusIndicator} from "evergreen-ui";
import Dashboard from "./dashboard";

export default function Index(props: any) {
    return (
        <Dashboard title={"Home"} user={props.user}>
            <div className={"boxContainer"}>
                <div className={"boxContentContainer"}>
                    <a className={"box1_3"}>
                        <div className={"box1_title"}>
                            QUICK
                        </div>
                    </a>
                </div>
                <div className={"boxContentContainer"}>
                    <a className={"box1_1"}>
                        <div className={`box1_content`}>
                            <div className={`box1_title`}>
                                Resource Summary
                            </div>
                            <div className={`box1_header`}>
                                Processing jobs
                            </div>

                            <div className={`box1_header`}>
                                <div className={`box1_description`}>
                                    <p><StatusIndicator color="success">Success</StatusIndicator> | Task: Image recognition</p>
                                    <p><StatusIndicator color="warning">In process</StatusIndicator> | Task: Clothing segmentation</p>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a className={`${HomeStyle.gradientNewsBackground} box1_1`}>
                        <div className={`box1_content`}>
                            <div className={`box1_title ${HomeStyle.newsTitleText}`}>
                                News
                            </div>
                            <div className={`box1_header`}>
                                <div className={`${HomeStyle.newsTitleText} box1_description`}>
                                    Introducing HESSIC Lighthouse
                                </div>
                            </div>
                            <div className={`box1_header`}>
                                <div className={`${HomeStyle.newsTitleText} box1_description`}>
                                    Learn how we manage your data
                                </div>
                            </div>
                        </div>
                    </a>
                    <a className={"box1_2"}>
                        <div className={"box1_content"}>
                            <div className={"box1_title"}>PROFILE</div>
                            <div className={"box1_header"}>
                                <div className={"box1_description"}>Welcome back, Kai Wen.</div>
                            </div>
                            <div className={"box1_header"}>
                                You are a user since: <div className={"box1_value"}>09/13/2020</div>
                            </div>
                            <div className={"box1_header"}>
                                Role: <div className={"box1_value"}>Analyst</div>
                            </div>
                            <div className={"box1_header"}>
                                Company: <div className={"box1_value"}>McKinsey & Company</div>
                            </div>
                            <div className={"box1_header"}>
                                License: <div className={"box1_value"}>Perpetual</div>
                            </div>
                            <div className={"box1_header"}>
                                Invite a collaborator: <div className={"box1_value"}><a href={""}>Invite</a></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>


    {/*<div className={style.container}>*/}

    {/*    <div className={"row center"}>*/}
    {/*        <div className={"cl--80"}>*/}
    {/*            <div className={style.banner}>*/}
    {/*                Early HESSIC Private Alpha for internal purpose only.*/}
    {/*                <b> Unauthorized attempts to share materials of the platform are strictly prohibited.</b>*/}
    {/*            </div>*/}
    {/*        </div>*/}
    {/*    </div>*/}

    {/*    <div className={"row dashboard"}>*/}

    {/*        <ul className={style.flex_container}>*/}
    {/*            <div className={style.projects}>*/}
    {/*                <div className={style.projects_title}>*/}
    {/*                    <div className={style.projects_title_item}>*/}
    {/*                        Projects*/}
    {/*                    </div>*/}
    {/*                    <div className={"right $(style.projects_title_item)"}>*/}
    {/*                        <Provider>*/}
    {/*                            <Tooltip>*/}
    {/*                                <TooltipTrigger asChild>*/}
    {/*                                    <IconButton>*/}
    {/*                                        <PlusIcon />*/}
    {/*                                    </IconButton>*/}
    {/*                                </TooltipTrigger>*/}
    {/*                                <TooltipContent sideOffset={5} >*/}
    {/*                                    Add to library*/}
    {/*                                </TooltipContent>*/}
    {/*                            </Tooltip>*/}
    {/*                        </Provider>*/}
    {/*                    </div>*/}
    {/*                </div>*/}
    {/*                <div className={style.projects_content}>All projects</div>*/}
    {/*            </div>*/}

    {/*            <div className={style.projects}>*/}
    {/*                <div className={style.activity_title}>*/}
    {/*                    <div className={style.projects_title_item}>*/}
    {/*                        Activity in the last six months*/}
    {/*                    </div>*/}
    {/*                </div>*/}
    {/*                <div className={style.activity_content}>*/}
    {/*                    <CalendarHeatmap*/}
    {/*                        startDate={new Date().setMonth(new Date().getMonth() - 6)}*/}
    {/*                        endDate={new Date().toISOString().slice(0, 10)}*/}
    {/*                        values={[*/}
    {/*                            { date: '2016-01-01', count: 12 },*/}
    {/*                            { date: '2016-01-22', count: 122 },*/}
    {/*                            { date: '2016-01-30', count: 38 },*/}
    {/*                        ]}*/}
    {/*                        tooltipDataAttrs={(value:any) => {*/}
    {/*                            if (value.date != null) {*/}
    {/*                                return {*/}
    {/*                                    'data-tip': value.date + " has count: " + value.count*/}
    {/*                                }*/}
    {/*                            }*/}
    {/*                        }}*/}
    {/*                    />*/}
    {/*                </div>*/}
    {/*            </div>*/}

    {/*            <div className={style.projects}>*/}
    {/*                <div className={style.projects_title}>*/}
    {/*                    <div className={style.projects_title_item}>*/}
    {/*                        Planned Maintenance*/}
    {/*                    </div>*/}
    {/*                </div>*/}
    {/*                <div className={style.activity_content}>*/}
    {/*                    No upcoming maintenances*/}
    {/*                </div>*/}
    {/*            </div>*/}


    {/*            <div className={style.projects}>*/}
    {/*                <div className={style.projects_title}>*/}
    {/*                    <div className={style.projects_title_item}>*/}
    {/*                        /!*<Progress value={66}>*!/*/}
    {/*                        /!*        <ProgressIndicator style={{ transform: `translateX(-${100 - progress}%)` }} />*!/*/}
    {/*                        /!*</Progress>*!/*/}
    {/*                        ok*/}
    {/*                    </div>*/}
    {/*                </div>*/}
    {/*                <div className={style.activity_content}>*/}
    {/*                </div>*/}
    {/*            </div>*/}
    {/*            <div className={style.projects}>*/}
    {/*                <div className={style.projects_title}>*/}
    {/*                    <div className={style.projects_title_item}>*/}
    {/*                        Planned Maintenance*/}
    {/*                    </div>*/}
    {/*                </div>*/}
    {/*                <div className={style.activity_content}>*/}
    {/*                    No upcoming maintenances*/}
    {/*                </div>*/}
    {/*            </div>*/}


    {/*            <div className={style.projects}>*/}
    {/*                <div className={style.projects_title}>*/}
    {/*                    <div className={style.projects_title_item}>*/}
    {/*                        /!*<Progress value={66}>*!/*/}
    {/*                        /!*        <ProgressIndicator style={{ transform: `translateX(-${100 - progress}%)` }} />*!/*/}
    {/*                        /!*</Progress>*!/*/}
    {/*                        ok*/}
    {/*                    </div>*/}
    {/*                </div>*/}
    {/*                <div className={style.activity_content}>*/}
    {/*                </div>*/}
    {/*            </div>*/}

    {/*        </ul>*/}
    {/*    </div>*/}
    {/*</div>*/}
    </Dashboard>
);
}
