import React from 'react';
import {Layout} from 'antd';
import style from "../../styles/components/Dashboard.module.scss"
import Sidebar from './sidebar';
import Navbar from "./navbar";
import ToolBar from "./toolBar";

export default function Dashboard(props: any) {
    return (
        <Layout>
            <Navbar user={props.user}/>
            <Sidebar/>
            <div className={style.PageViewContainer}>
                <ToolBar title={props.title}/>
                <div className={"pageViewContent"}>
                    {props.children}
                </div>
            </div>
        </Layout>
    );
}
