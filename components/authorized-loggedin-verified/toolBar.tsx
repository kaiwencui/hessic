import React, {useEffect} from 'react';
import style from "../../styles/components/Toolbar.module.scss";
import { Cross1Icon, RowsIcon} from '@radix-ui/react-icons';


import {useUser} from "../../lib/hooks";

export default function ToolBar(props: any) {

    const [isMenuClosed, setMenuClosed] = React.useState(true);

    const showMenu = () => {
        let element = document.getElementById('sidebar')
        if (element) {
            if (element.style.left === '') {
                element.style.left = '0'
                setMenuClosed(false)
            } else {
                element.style.left = ''
                setMenuClosed(true)
            }
        }
    }

    const { user } = useUser({})

    useEffect(() => {
    }, [user])


    // Loading state
    if (user.isLoggedIn === undefined) {
        return <></>
    }

    return (
        <div>
            <div className={style.toolbarSpacerContainerMobile}>
                <div className={style.toolbarMobileLeft}>
                    <div className={style.toolbarMobileButtonMenu} onClick={()=>showMenu()}>
                        {isMenuClosed ? (<RowsIcon/>): (<Cross1Icon/>)}
                    </div>
                    <div className={style.toolbarMobileTitle}>
                        {props.title}
                    </div>
                </div>
            </div>
            <div className={style.toolbarSpacerContainer}>
                {props.title}
            </div>
        </div>

    );
}
