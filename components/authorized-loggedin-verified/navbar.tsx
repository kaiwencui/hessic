import React, {useCallback} from 'react';
import style from "../../styles/components/Navbar.module.scss";
import { styled, keyframes } from '@stitches/react';
import { violet, mauve, blackA } from '@radix-ui/colors';
import {
    AvatarIcon, BellIcon,
    ChevronRightIcon, ExitIcon, GlobeIcon, Half2Icon, MixerVerticalIcon, PinBottomIcon, PlusIcon,
} from '@radix-ui/react-icons';
import * as DropdownMenuPrimitive from '@radix-ui/react-dropdown-menu';
import {LightningIcon} from "evergreen-ui";
import Link from "next/link";
import Router from "next/router";
import {logoutUser} from "../../lib/user";

/**
 * Start Radix UI Dropdown Menu
 */
const slideUpAndFade = keyframes({
    '0%': { opacity: 0, transform: 'translateY(2px)' },
    '100%': { opacity: 1, transform: 'translateY(0)' },
});

const slideRightAndFade = keyframes({
    '0%': { opacity: 0, transform: 'translateX(-2px)' },
    '100%': { opacity: 1, transform: 'translateX(0)' },
});

const slideDownAndFade = keyframes({
    '0%': { opacity: 0, transform: 'translateY(-2px)' },
    '100%': { opacity: 1, transform: 'translateY(0)' },
});

const slideLeftAndFade = keyframes({
    '0%': { opacity: 0, transform: 'translateX(2px)' },
    '100%': { opacity: 1, transform: 'translateX(0)' },
});

const contentStyles = {
    minWidth: 220,
    backgroundColor: 'rgb(27,30,32)',
    borderRadius: 6,
    padding: 5,
    boxShadow:
        '0px 10px 38px -10px rgba(22, 23, 24, 0.35), 0px 10px 20px -15px rgba(22, 23, 24, 0.2)',
    '@media (prefers-reduced-motion: no-preference)': {
        animationDuration: '400ms',
        animationTimingFunction: 'cubic-bezier(0.16, 1, 0.3, 1)',
        willChange: 'transform, opacity',
        '&[data-state="open"]': {
            '&[data-side="top"]': { animationName: slideDownAndFade },
            '&[data-side="right"]': { animationName: slideLeftAndFade },
            '&[data-side="bottom"]': { animationName: slideUpAndFade },
            '&[data-side="left"]': { animationName: slideRightAndFade },
        },
    },
};

const StyledContent = styled(DropdownMenuPrimitive.Content, { ...contentStyles });

const StyledArrow = styled(DropdownMenuPrimitive.Arrow, {
    fill: 'white',
});

function Content({ children, ...props }:any) {
    return (
        <DropdownMenuPrimitive.Portal>
            <StyledContent {...props}>
                {children}
                <StyledArrow />
            </StyledContent>
        </DropdownMenuPrimitive.Portal>
    );
}

const StyledSubContent = styled(DropdownMenuPrimitive.SubContent, { ...contentStyles });

function SubContent(props:any) {
    return (
        <DropdownMenuPrimitive.Portal>
            <StyledSubContent {...props} />
        </DropdownMenuPrimitive.Portal>
    );
}

const itemStyles = {
    all: 'unset',
    fontSize: 13,
    lineHeight: 1,
    color: '#ffffff',
    borderRadius: 3,
    display: 'flex',
    alignItems: 'center',
    height: 25,
    padding: '0 5px',
    position: 'relative',
    paddingLeft: 25,
    userSelect: 'none',

    '&[data-disabled]': {
        color: mauve.mauve8,
        pointerEvents: 'none',
    },

    '&[data-highlighted]': {
        backgroundColor: '#000000',
        color: '#ffffff',
    },
};

const StyledItem = styled(DropdownMenuPrimitive.Item, { ...itemStyles });
const StyledSubTrigger = styled(DropdownMenuPrimitive.SubTrigger, {
    '&[data-state="open"]': {
        backgroundColor: '#000000',
        color: '#ffffff',
    },
    ...itemStyles,
});

const StyledSeparator = styled(DropdownMenuPrimitive.Separator, {
    height: 1,
    backgroundColor: '#b9b9b9',
    margin: 5,
});

export const DropdownMenu = DropdownMenuPrimitive.Root;
export const DropdownMenuTrigger = DropdownMenuPrimitive.Trigger;
export const DropdownMenuContent = Content;
export const DropdownMenuItem = StyledItem;
export const DropdownMenuSeparator = StyledSeparator;
export const DropdownMenuSub = DropdownMenuPrimitive.Sub;
export const DropdownMenuSubTrigger = StyledSubTrigger;
export const DropdownMenuSubContent = SubContent;

const RightSlot = styled('div', {
    marginLeft: 'auto',
    paddingLeft: 20,
    color: mauve.mauve11,
    '[data-highlighted] > &': { color: 'white' },
    '[data-disabled] &': { color: mauve.mauve8 },
});

const IconButton = styled('button', {
    all: 'unset',
    fontFamily: 'inherit',
    borderRadius: '100%',
    height: 24,
    width: 24,
    display: 'inline-flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: violet.violet11,
    backgroundColor: 'white',
    boxShadow: `0 2px 10px ${blackA.blackA7}`,
    '&:hover': { backgroundColor: violet.violet3 },
    '&:focus': { boxShadow: `0 0 0 2px black` },
});




export default function Navbar(props: any) {

    const [isOpen, setOpen] = React.useState(false);
    let user = props.user;
    const ref = useCallback((node:any) => {
        if (node !== null) {
            if (node.dataset.state === 'open') {
                setOpen(true);
            } else {
                setOpen(false)
            }
        }
    }, []);

    // Logout
    const logout = async () => {
        let res = await logoutUser();
        if (res)
            Router.reload()
    };

    return (
        <div className={style.navbarContainer}>
            {/* Left side */}
            <div>
                {/* User */}
                <div className={`${style.navbarContentContainer} ${style.leftNav}`} tabIndex={0}>
                    <div className={`${style.navbarContent} ${style.leftNav}`}>
                        <GlobeIcon className={style.workSpaceIcon}/>
                        <div className={style.workSpaceName}>
                            <div className={style.workSpaceNameTitleSmaller}>{user.firstname} {user.lastname}</div>
                            <div className={style.workSpaceNameTitleSubtitle}>{user.work_email}</div>
                        </div>
                        <div className={`${style.smallIconContainer} ${style.leftNav}`}>
                            <svg className={style.small} width="8" height="7" viewBox="0 0 8 7" xmlns="http://www.w3.org/2000/svg">
                                <path d="M3.646 5.354l-3-3 .708-.708L4 4.293l2.646-2.647.708.708-3 3L4 5.707l-.354-.353z"
                                      fill="#000" stroke="none"></path>
                            </svg>
                        </div>
                    </div>
                </div>

            </div>

            {/* Right side */}
            <div>
                {/* Notification */}
                <div className={style.navbarContentContainer}
                     tabIndex={0}>
                    <div className={style.navbarContent}>
                        <BellIcon/>
                    </div>
                </div>

                {/* DropdownMenu */}
                <div className={`${isOpen ? style.navbarContentContainerActive : ''} ${style.navbarContentContainer} ${style.right}`} tabIndex={1}>
                        <DropdownMenu>
                            <DropdownMenuTrigger asChild ref={ref}>
                                <div className={`${style.navbarContent} ${style.buttonUser}`}>
                                    <IconButton>
                                        {user.work_email.charAt(0).toUpperCase()}
                                    </IconButton>
                                    <div className={style.smallIconContainer}>
                                        <svg className={style.small} width="8" height="7" viewBox="0 0 8 7" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M3.646 5.354l-3-3 .708-.708L4 4.293l2.646-2.647.708.708-3 3L4 5.707l-.354-.353z"
                                                  fill="#000" stroke="none"></path>
                                        </svg>
                                    </div>
                                </div>
                            </DropdownMenuTrigger>
                            <DropdownMenuContent sideOffset={5} className={style.dropdownMenu}>
                                <div className={style.dropdownMenuUsername}>
                                    <div className={style.dropdownMenuUsernameIcon}>
                                        <IconButton aria-label="Customise options">
                                            {user.work_email.charAt(0).toUpperCase()}
                                        </IconButton>
                                    </div>
                                    <div className={style.dropdownMenuUsernameEmail}>
                                        {user.work_email}
                                    </div>
                                </div>
                                <DropdownMenuItem className={style.dropdownMenuItem}>
                                    <AvatarIcon className={style.dropdownIcon}/> Internal Profile
                                </DropdownMenuItem>
                                <DropdownMenuItem className={style.dropdownMenuItem}>
                                    <LightningIcon className={style.dropdownIcon}/> Plugins
                                </DropdownMenuItem>
                                <DropdownMenuItem className={style.dropdownMenuItem}>
                                    <MixerVerticalIcon className={style.dropdownIcon}/> Settings
                                </DropdownMenuItem>
                                <DropdownMenuSub>
                                    <DropdownMenuSubTrigger className={style.dropdownMenuItem}>
                                        <Half2Icon className={style.dropdownIcon}/> Theme
                                        <RightSlot>
                                            <ChevronRightIcon />
                                        </RightSlot>
                                    </DropdownMenuSubTrigger>
                                    <DropdownMenuSubContent sideOffset={2} alignOffset={-5}>
                                        <DropdownMenuItem>
                                            Light
                                        </DropdownMenuItem>
                                        <DropdownMenuItem>Dark</DropdownMenuItem>
                                        <DropdownMenuItem>System Theme</DropdownMenuItem>
                                    </DropdownMenuSubContent>
                                </DropdownMenuSub>
                                <DropdownMenuItem className={style.dropdownMenuItem}>
                                    <PinBottomIcon className={style.dropdownIcon}/> Get Desktop App
                                </DropdownMenuItem>
                                <DropdownMenuSeparator />
                                <DropdownMenuItem className={style.dropdownMenuItem}>
                                    <PlusIcon className={style.dropdownIcon}/> Add account
                                </DropdownMenuItem>

                                <DropdownMenuItem className={style.dropdownMenuItem} onClick={()=>logout()}>
                                    <ExitIcon className={style.dropdownIcon}/>Logout
                                </DropdownMenuItem>

                            </DropdownMenuContent>
                        </DropdownMenu>
                </div>
            </div>
        </div>
    );
}
