/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  env: {
    FRONT_END_URL: "http://localhost:3000",
    REACT_APP_BACKEND_URL: "http://localhost:4000",
    REACT_APP_API_URL: "http://localhost:5000",
    DEV: true
  },
  headers: [
    {
      key: 'Access-Control-Allow-Origin',
      value: '*',
    },
    {
      key: 'Access-Control-Allow-Credentials',
      value: true,
    },
  ],
}

module.exports = nextConfig


// /** @type {import('next').NextConfig} */
// module.exports = {
//   webpack: (config, options) => {
//     // Important: return the modified config
//     config.module.rules.push({
//       test: /\.scss$/,
//       use: [
//         "style-loader", // 3. Inject styles into DOM
//         "css-loader", // 2. Turns css into commonjs
//         "sass-loader", // 1. Turns sass into css
//       ],
//     })
//
//
//     return config
//   },
//   reactStrictMode: true,
//   swcMinify: true,
// }