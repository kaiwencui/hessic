import Router, {useRouter} from 'next/router';
import {useEffect, useState} from "react";

/**
 * Handles front-end user authentication.
 *
 * @param redirectTo - The path to follow given redirectIfFound and authentication status
 * @param redirectIfFound - If redirectIfFound is set to false and the user is not logged in, then follow redirectTo
 *                          If redirectIfFound is set to true and the user is logged in, then follow redirectTo
 *
 * @returns User information if user is authenticated, otherwise null
 *
 * @beta
 */
export function useUser({redirectTo = '', redirectIfFound = false}: any = {}) {

    let router = useRouter()

    const [user, setUser] = useState({
        isLoggedIn: undefined,
        isAdmin: undefined,
        isVerified: undefined
    });

    useEffect(() => {

        /**
         * HTTP request given an url
         *
         * @param url - The url to fetch
         *
         * @returns User info if authenticated, otherwise null
         *
         * @beta
         */
        fetch(process.env.REACT_APP_BACKEND_URL + '/api/user', {
            credentials: 'include' // send cookies to url
        })
            .then((r) => r.json())
            .then((data) => {
                setUser(data?.user)
                // if no redirect needed, just return (example: already on /dashboard)
                // if user data not yet there (fetch in progress, logged in or not) then don't do anything yet
                if (!redirectTo || !data?.user) return
                if (
                    (redirectTo && !redirectIfFound && !data?.user?.isLoggedIn) || // if redirectTo is set, redirect if the user was not found.
                    (redirectIfFound && data?.user?.isLoggedIn) // if redirectIfFound is also set, redirect if the user was found
                ) {
                    Router.push(redirectTo, undefined, {shallow:false});
                }
            })
    }, [router.pathname, redirectTo, redirectIfFound])

    return { user }
}
