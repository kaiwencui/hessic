import React from "react";

/**
 * Logs out user
 *
 * @remarks
 * This function logs out the user by removing the token cookie
 *
 * @returns res - Object to send back the desired HTTP response
 *
 * @beta
 */
export async function logoutUser() {
    const res = await fetch(process.env.REACT_APP_BACKEND_URL + '/api/logout', {
        credentials: 'include' // send cookies to url
    })
    return res
};